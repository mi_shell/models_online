$(function () {

    /*global variables*/
    var scene, camera, renderer;
    var controls, guicontrols, datGUI;
    var light1, light2, light3, light4, ambient;
    var geometry, material, sphere1, sphere2, sphere3, sphere4;
    var axisHelper;
    var loader;
    var SCREEN_WIDTH, SCREEN_HEIGHT;
    var mouse, raycaster, intersects;
    var objects = [];

    function init() {
        $(".popup").hide();
        $("#popup2").hide();

        /*creates empty scene object and renderer*/
        scene = new THREE.Scene();
        camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 0.1, 100);
        renderer = new THREE.WebGLRenderer({
            antialias: true,
            alpha: true
        });

        renderer.setSize(window.innerWidth, window.innerHeight);
        //renderer.shadowMapEnabled = false;
        //renderer.shadowMapSoft = false;

        /*add controls*/
        controls = new THREE.OrbitControls(camera, renderer.domElement);
        controls.addEventListener('change', render);

        camera.position.x = 12.5;
        camera.position.y = 5;
        camera.position.z = -8.75;
        camera.lookAt(scene.position);

        THREEx.WindowResize(renderer, camera);
        THREEx.FullScreen.bindKey({
            charCode: 'm'.charCodeAt(0)
        });

        /*datGUI controls object*/
        guicontrols = new function () {
            this.explodeModel = function () {
                console.log(scene);
            };
            this.explode = 0;
        }

        light1 = new THREE.PointLight(0x7d7d7d);
        light1.position.set(0, 10, -10);
        scene.add(light1);

        light2 = new THREE.PointLight(0x7d7d7d);
        light2.position.set(-10, 0, 0);
        scene.add(light2);

        light3 = new THREE.PointLight(0x7d7d7d);
        light3.position.set(0, -10, -10);
        scene.add(light3);

        light4 = new THREE.PointLight(0x7d7d7d);
        light4.position.set(0, -10, 10);
        scene.add(light4);

        ambient = new THREE.AmbientLight(0xffffff);
        scene.add(ambient);

        //spheres to help with the positioning of the lights
        /*
        geometry = new THREE.SphereGeometry(50, 32, 32);
        material = new THREE.MeshBasicMaterial({
            color: 0xffff00
        });
        sphere1 = new THREE.Mesh(geometry, material);
        sphere1.position.set(-10, 0, 0)
        scene.add(sphere1);

        sphere2 = new THREE.Mesh(geometry, material);
        sphere2.position.set(0, 10, 10)
        scene.add(sphere2);

        sphere3 = new THREE.Mesh(geometry, material);
        sphere3.position.set(0, -10, -10)
        scene.add(sphere3);

        sphere4 = new THREE.Mesh(geometry, material);
        sphere4.position.set(0, -10, 10)
        scene.add(sphere4);
        */

        //axisHelper to help with the positioning of the exploded parts
        /*
        axisHelper = new THREE.AxisHelper(10);
        scene.add(axisHelper);
        */
        
        loader = new THREE.ObjectLoader();
        loader.load("models/modular_studio.json", function (obj) {

            obj.scale.x = 15;
            obj.scale.y = 15;
            obj.scale.z = 15;
            obj.position.x = 0;
            obj.position.y = 0;
	    obj.position.z = 0;
            scene.add(obj);

            scene.traverse(function (children) {
                objects.push(children);
            });

	crbeamcorner = obj.getObjectByName("CR beam corner", true);
        crbeamfloor01 = obj.getObjectByName("CR beam floor 01", true);
        crbeamfloor02 = obj.getObjectByName("CR beam floor 02", true);
        crbeamfloorshort01 = obj.getObjectByName("CR beam floor short 01", true);
        crbeamfloorshort02 = obj.getObjectByName("CR beam floor short 02", true);
        crbeamroof01 = obj.getObjectByName("CR beam roof 01", true);
        crbeamroof02 = obj.getObjectByName("CR beam roof 02", true);
        crbeamroofshort01 = obj.getObjectByName("CR beam roof short 01", true);
        crbeamroofshort02 = obj.getObjectByName("CR beam roof short 02", true);
        crstudiopaneldoor01 = obj.getObjectByName("CR studio panel door 01", true);
        crstudiopanelfloor01 = obj.getObjectByName("CR studio panel floor 01", true);
        crstudiopanelroof01 = obj.getObjectByName("CR studio panel roof 01", true);
        crstudiopanelwall01 = obj.getObjectByName("CR studio panel wall 01", true);
        crstudiopanelwall02 = obj.getObjectByName("CR studio panel wall 02", true);
        crstudiopanelwall03 = obj.getObjectByName("CR studio panel wall 03", true);
        rrbeamfloor01 = obj.getObjectByName("RR beam floor 01", true);
        rrbeamfloor02 = obj.getObjectByName("RR beam floor 02", true);
        rrbeamfloor03 = obj.getObjectByName("RR beam floor 03", true);
        rrbeamfloor04 = obj.getObjectByName("RR beam floor 04", true);
        rrbeamroof01 = obj.getObjectByName("RR beam roof 01", true);
        rrbeamroof02 = obj.getObjectByName("RR beam roof 02", true);
        rrbeamroof03 = obj.getObjectByName("RR beam roof 03", true);
        rrbeamroof04 = obj.getObjectByName("RR beam roof 04", true);
        rrosb39mm01 = obj.getObjectByName("RR osb3 9mm 01", true);
        rrosb39mm02 = obj.getObjectByName("RR osb3 9mm 02", true);
        rrrockfloorsolid01 = obj.getObjectByName("RR rockfloor solid 01", true);
        rrrockfloorsolid02 = obj.getObjectByName("RR rockfloor solid 02", true);
        rrrockfloorsolid03 = obj.getObjectByName("RR rockfloor solid 03", true);
        rrrockfloorsolid04 = obj.getObjectByName("RR rockfloor solid 04", true);
        rrrockfloorsolid05 = obj.getObjectByName("RR rockfloor solid 05", true);
        rrrockfloorsolid06 = obj.getObjectByName("RR rockfloor solid 06", true);
        rrrockfloorsolid07 = obj.getObjectByName("RR rockfloor solid 07", true);
        rrrockfloorsolid08 = obj.getObjectByName("RR rockfloor solid 08", true);
        rrrockfloorsolid09 = obj.getObjectByName("RR rockfloor solid 09", true);
        rrrockfloorsolid10 = obj.getObjectByName("RR rockfloor solid 10", true);
        rrrockfloorsolid11 = obj.getObjectByName("RR rockfloor solid 11", true);
        rrstudiopaneldoor01 = obj.getObjectByName("RR studio panel door 01", true);
        rrstudiopanelfloor01 = obj.getObjectByName("RR studio panel floor 01", true);
        rrstudiopanelfloor02 = obj.getObjectByName("RR studio panel floor 02", true);
        rrstudiopanelroof01 = obj.getObjectByName("RR studio panel roof 01", true);
        rrstudiopanelroof02 = obj.getObjectByName("RR studio panel roof 02", true);
        rrstudiopanelwall01 = obj.getObjectByName("RR studio panel wall 01", true);
        rrstudiopanelwall02 = obj.getObjectByName("RR studio panel wall 02", true);
        rrstudiopanelwall03 = obj.getObjectByName("RR studio panel wall 03", true);
        rrstudiopanelwall04 = obj.getObjectByName("RR studio panel wall 04", true);
        rrstudiopanelwall05 = obj.getObjectByName("RR studio panel wall 05", true);
        rrstudiopanelwall06 = obj.getObjectByName("RR studio panel wall 06", true);
        rrstudiopanelwindow01 = obj.getObjectByName("RR studio panel window 01", true);


        });

        //add raycaster and mouse as 2D vector
        raycaster = new THREE.Raycaster();
        mouse = new THREE.Vector2();

        //add event listener for mouse and calls function when activated
        document.addEventListener('dblclick', onDoubleClick, false);
        document.addEventListener('mousemove', onDocumentMouseMove, false);
        document.addEventListener('touchstart', onDocumentTouchStart, false);

        /*adds controls to scene*/
        datGUI = new dat.GUI();
        datGUI.add(guicontrols, 'explodeModel');

        datGUI.add(guicontrols, 'explode', 0, 1);

        guicontrols.roof = true;
        datGUI.add(guicontrols, "roof").name("Roof");

        guicontrols.controleroom = true;
        datGUI.add(guicontrols, "controleroom").name("Controle Room");

        guicontrols.recordingroom = true;
        datGUI.add(guicontrols, "recordingroom").name("Recording Room");

        guicontrols.floatingfloor = true;
        datGUI.add(guicontrols, "floatingfloor").name("Floating floor");



        $("#webGL-container").append(renderer.domElement);

    }

    function onDocumentTouchStart(event) {

        event.preventDefault();

        event.clientX = event.touches[0].clientX;
        event.clientY = event.touches[0].clientY;
        onDoubleClick(event);
        onDocumentMouseMove(event);

    }

    function onDoubleClick(event) {

        event.preventDefault();

        mouse.x = (event.clientX / renderer.domElement.width) * 2 - 1;
        mouse.y = -(event.clientY / renderer.domElement.height) * 2 + 1;

        raycaster.setFromCamera(mouse, camera);

        intersects = raycaster.intersectObjects(objects);

        if (intersects.length > 0) {

            this.userData = intersects[0].object.userData;
            this.name = intersects[0].object.name;

            $(".text").empty();
            $(".popup").append("<div class='text'><p> <strong>" + this.name + "</strong>" + this.userData + "</div>");
            $(".popup").show();

        }

    }

    function onDocumentMouseMove(event) {

        event.preventDefault();

        mouse.x = (event.clientX / renderer.domElement.width) * 2 - 1;
        mouse.y = -(event.clientY / renderer.domElement.height) * 2 + 1;

        raycaster.setFromCamera(mouse, camera);

        var intersects = raycaster.intersectObjects(objects);

        if (intersects.length > 0) {

            this.name = intersects[0].object.name;

            $(".text2").empty();
            $("#popup2").append("<div class='text2'><p> <strong>" + this.name + "</strong></p></div>");
            $("#popup2").show();

        } else {
            $("#popup2").hide();

        }
    }

    function render() {
        if (typeof crbeamcorner !== "undefined") {
          crbeamcorner.position.x = guicontrols.explode * 0.169;
          crbeamcorner.position.y = guicontrols.explode * -0.002;
          crbeamcorner.position.z = guicontrols.explode * 0.108;
          crbeamcorner.visible = guicontrols.controleroom;
        }
        if (typeof crbeamfloor01 !== "undefined") {
          crbeamfloor01.position.x = guicontrols.explode * 0.057;
          crbeamfloor01.position.y = guicontrols.explode * -0.115;
          crbeamfloor01.position.z = guicontrols.explode * 0.002;
          crbeamfloor01.visible = guicontrols.controleroom;
        }
        if (typeof crbeamfloor02 !== "undefined") {
          crbeamfloor02.position.x = guicontrols.explode * 0.171;
          crbeamfloor02.position.y = guicontrols.explode * -0.115;
          crbeamfloor02.position.z = guicontrols.explode * -0.003;
          crbeamfloor02.visible = guicontrols.controleroom;
        }
        if (typeof crbeamfloorshort01 !== "undefined") {
          crbeamfloorshort01.position.x = guicontrols.explode * 0.112;
          crbeamfloorshort01.position.y = guicontrols.explode * -0.115;
          crbeamfloorshort01.position.z = guicontrols.explode * -0.111;
          crbeamfloorshort01.visible = guicontrols.controleroom;
        }
        if (typeof crbeamfloorshort02 !== "undefined") {
          crbeamfloorshort02.position.x = guicontrols.explode * 0.117;
          crbeamfloorshort02.position.y = guicontrols.explode * -0.115;
          crbeamfloorshort02.position.z = guicontrols.explode * 0.111;
          crbeamfloorshort02.visible = guicontrols.controleroom;
        }
        if (typeof crbeamroof01 !== "undefined") {
          crbeamroof01.position.x = guicontrols.explode * 0.057;
          crbeamroof01.position.y = guicontrols.explode * 0.112;
          crbeamroof01.position.z = guicontrols.explode * 0.002;
          crbeamroof01.visible = guicontrols.roof;
        }
        if (typeof crbeamroof02 !== "undefined") {
          crbeamroof02.position.x = guicontrols.explode * 0.171;
          crbeamroof02.position.y = guicontrols.explode * 0.112;
          crbeamroof02.position.z = guicontrols.explode * -0.003;
          crbeamroof02.visible = guicontrols.roof;
        }
        if (typeof crbeamroofshort01 !== "undefined") {
          crbeamroofshort01.position.x = guicontrols.explode * 0.112;
          crbeamroofshort01.position.y = guicontrols.explode * 0.112;
          crbeamroofshort01.position.z = guicontrols.explode * -0.111;
          crbeamroofshort01.visible = guicontrols.roof;
        }
        if (typeof crbeamroofshort02 !== "undefined") {
          crbeamroofshort02.position.x = guicontrols.explode * 0.117;
          crbeamroofshort02.position.y = guicontrols.explode * 0.112;
          crbeamroofshort02.position.z = guicontrols.explode * 0.111;
          crbeamroofshort02.visible = guicontrols.roof;
        }
        if (typeof crstudiopaneldoor01 !== "undefined") {
          crstudiopaneldoor01.position.x = guicontrols.explode * 0.169;
          crstudiopaneldoor01.position.y = guicontrols.explode * -0.01;
          crstudiopaneldoor01.position.z = guicontrols.explode * -0.06;
          crstudiopaneldoor01.visible = guicontrols.controleroom;
        }
        if (typeof crstudiopanelfloor01 !== "undefined") {
          crstudiopanelfloor01.position.x = guicontrols.explode * 0.114;
          crstudiopanelfloor01.position.y = guicontrols.explode * -0.115;
          crstudiopanelfloor01.position.z = guicontrols.explode * -0.0;
          crstudiopanelfloor01.visible = guicontrols.controleroom;
        }
        if (typeof crstudiopanelroof01 !== "undefined") {
          crstudiopanelroof01.position.x = guicontrols.explode * 0.114;
          crstudiopanelroof01.position.y = guicontrols.explode * 0.112;
          crstudiopanelroof01.position.z = guicontrols.explode * -0.0;
          crstudiopanelroof01.visible = guicontrols.roof;
        }
        if (typeof crstudiopanelwall01 !== "undefined") {
          crstudiopanelwall01.position.x = guicontrols.explode * 0.109;
          crstudiopanelwall01.position.y = guicontrols.explode * -0.002;
          crstudiopanelwall01.position.z = guicontrols.explode * -0.109;
          crstudiopanelwall01.visible = guicontrols.controleroom;
        }
        if (typeof crstudiopanelwall02 !== "undefined") {
          crstudiopanelwall02.position.x = guicontrols.explode * 0.109;
          crstudiopanelwall02.position.y = guicontrols.explode * -0.002;
          crstudiopanelwall02.position.z = guicontrols.explode * 0.108;
          crstudiopanelwall02.visible = guicontrols.controleroom;
        }
        if (typeof crstudiopanelwall03 !== "undefined") {
          crstudiopanelwall03.position.x = guicontrols.explode * 0.169;
          crstudiopanelwall03.position.y = guicontrols.explode * -0.002;
          crstudiopanelwall03.position.z = guicontrols.explode * 0.049;
          crstudiopanelwall03.visible = guicontrols.controleroom;
        }
        if (typeof rrbeamfloor01 !== "undefined") {
          rrbeamfloor01.position.x = guicontrols.explode * 0.052;
          rrbeamfloor01.position.y = guicontrols.explode * -0.115;
          rrbeamfloor01.position.z = guicontrols.explode * -0.003;
          rrbeamfloor01.visible = guicontrols.recordingroom;
        }
        if (typeof rrbeamfloor02 !== "undefined") {
          rrbeamfloor02.position.x = guicontrols.explode * -0.056;
          rrbeamfloor02.position.y = guicontrols.explode * -0.115;
          rrbeamfloor02.position.z = guicontrols.explode * 0.111;
          rrbeamfloor02.visible = guicontrols.recordingroom;
        }
        if (typeof rrbeamfloor03 !== "undefined") {
          rrbeamfloor03.position.x = guicontrols.explode * -0.17;
          rrbeamfloor03.position.y = guicontrols.explode * -0.115;
          rrbeamfloor03.position.z = guicontrols.explode * 0.002;
          rrbeamfloor03.visible = guicontrols.recordingroom;
        }
        if (typeof rrbeamfloor04 !== "undefined") {
          rrbeamfloor04.position.x = guicontrols.explode * -0.062;
          rrbeamfloor04.position.y = guicontrols.explode * -0.115;
          rrbeamfloor04.position.z = guicontrols.explode * -0.111;
          rrbeamfloor04.visible = guicontrols.recordingroom;
        }
        if (typeof rrbeamroof01 !== "undefined") {
          rrbeamroof01.position.x = guicontrols.explode * 0.052;
          rrbeamroof01.position.y = guicontrols.explode * 0.112;
          rrbeamroof01.position.z = guicontrols.explode * -0.003;
          rrbeamroof01.visible = guicontrols.roof;
        }
        if (typeof rrbeamroof02 !== "undefined") {
          rrbeamroof02.position.x = guicontrols.explode * -0.056;
          rrbeamroof02.position.y = guicontrols.explode * 0.112;
          rrbeamroof02.position.z = guicontrols.explode * 0.111;
          rrbeamroof02.visible = guicontrols.roof;
        }
        if (typeof rrbeamroof03 !== "undefined") {
          rrbeamroof03.position.x = guicontrols.explode * -0.17;
          rrbeamroof03.position.y = guicontrols.explode * 0.112;
          rrbeamroof03.position.z = guicontrols.explode * 0.002;
          rrbeamroof03.visible = guicontrols.roof;
        }
        if (typeof rrbeamroof04 !== "undefined") {
          rrbeamroof04.position.x = guicontrols.explode * -0.062;
          rrbeamroof04.position.y = guicontrols.explode * 0.112;
          rrbeamroof04.position.z = guicontrols.explode * -0.111;
          rrbeamroof04.visible = guicontrols.roof;
        }
        if (typeof rrosb39mm01 !== "undefined") {
          rrosb39mm01.position.x = guicontrols.explode * -0.008;
          rrosb39mm01.position.y = guicontrols.explode * -0.086;
          rrosb39mm01.position.z = guicontrols.explode * -0.0;
          rrosb39mm01.visible = guicontrols.floatingfloor;
        }
        if (typeof rrosb39mm02 !== "undefined") {
          rrosb39mm02.position.x = guicontrols.explode * -0.11;
          rrosb39mm02.position.y = guicontrols.explode * -0.086;
          rrosb39mm02.position.z = guicontrols.explode * -0.0;
          rrosb39mm02.visible = guicontrols.floatingfloor;
        }
        if (typeof rrrockfloorsolid01 !== "undefined") {
          rrrockfloorsolid01.position.x = guicontrols.explode * -0.0;
          rrrockfloorsolid01.position.y = guicontrols.explode * -0.098;
          rrrockfloorsolid01.position.z = guicontrols.explode * 0.075;
          rrrockfloorsolid01.visible = guicontrols.floatingfloor;
        }
        if (typeof rrrockfloorsolid02 !== "undefined") {
          rrrockfloorsolid02.position.x = guicontrols.explode * -0.0;
          rrrockfloorsolid02.position.y = guicontrols.explode * -0.098;
          rrrockfloorsolid02.position.z = guicontrols.explode * 0.019;
          rrrockfloorsolid02.visible = guicontrols.floatingfloor;
        }
        if (typeof rrrockfloorsolid03 !== "undefined") {
          rrrockfloorsolid03.position.x = guicontrols.explode * -0.0;
          rrrockfloorsolid03.position.y = guicontrols.explode * -0.098;
          rrrockfloorsolid03.position.z = guicontrols.explode * -0.036;
          rrrockfloorsolid03.visible = guicontrols.floatingfloor;
        }
        if (typeof rrrockfloorsolid04 !== "undefined") {
          rrrockfloorsolid04.position.x = guicontrols.explode * -0.0;
          rrrockfloorsolid04.position.y = guicontrols.explode * -0.098;
          rrrockfloorsolid04.position.z = guicontrols.explode * -0.084;
          rrrockfloorsolid04.visible = guicontrols.floatingfloor;
        }
        if (typeof rrrockfloorsolid05 !== "undefined") {
          rrrockfloorsolid05.position.x = guicontrols.explode * -0.089;
          rrrockfloorsolid05.position.y = guicontrols.explode * -0.098;
          rrrockfloorsolid05.position.z = guicontrols.explode * 0.083;
          rrrockfloorsolid05.visible = guicontrols.floatingfloor;
        }
        if (typeof rrrockfloorsolid06 !== "undefined") {
          rrrockfloorsolid06.position.x = guicontrols.explode * -0.089;
          rrrockfloorsolid06.position.y = guicontrols.explode * -0.098;
          rrrockfloorsolid06.position.z = guicontrols.explode * 0.035;
          rrrockfloorsolid06.visible = guicontrols.floatingfloor;
        }
        if (typeof rrrockfloorsolid07 !== "undefined") {
          rrrockfloorsolid07.position.x = guicontrols.explode * -0.089;
          rrrockfloorsolid07.position.y = guicontrols.explode * -0.098;
          rrrockfloorsolid07.position.z = guicontrols.explode * -0.02;
          rrrockfloorsolid07.visible = guicontrols.floatingfloor;
        }
        if (typeof rrrockfloorsolid08 !== "undefined") {
          rrrockfloorsolid08.position.x = guicontrols.explode * -0.089;
          rrrockfloorsolid08.position.y = guicontrols.explode * -0.098;
          rrrockfloorsolid08.position.z = guicontrols.explode * -0.076;
          rrrockfloorsolid08.visible = guicontrols.floatingfloor;
        }
        if (typeof rrrockfloorsolid09 !== "undefined") {
          rrrockfloorsolid09.position.x = guicontrols.explode * -0.148;
          rrrockfloorsolid09.position.y = guicontrols.explode * -0.098;
          rrrockfloorsolid09.position.z = guicontrols.explode * 0.058;
          rrrockfloorsolid09.visible = guicontrols.floatingfloor;
        }
        if (typeof rrrockfloorsolid10 !== "undefined") {
          rrrockfloorsolid10.position.x = guicontrols.explode * -0.148;
          rrrockfloorsolid10.position.y = guicontrols.explode * -0.098;
          rrrockfloorsolid10.position.z = guicontrols.explode * -0.031;
          rrrockfloorsolid10.visible = guicontrols.floatingfloor;
        }
        if (typeof rrrockfloorsolid11 !== "undefined") {
          rrrockfloorsolid11.position.x = guicontrols.explode * -0.148;
          rrrockfloorsolid11.position.y = guicontrols.explode * -0.098;
          rrrockfloorsolid11.position.z = guicontrols.explode * -0.089;
          rrrockfloorsolid11.visible = guicontrols.floatingfloor;
        }
        if (typeof rrstudiopaneldoor01 !== "undefined") {
          rrstudiopaneldoor01.position.x = guicontrols.explode * 0.049;
          rrstudiopaneldoor01.position.y = guicontrols.explode * -0.01;
          rrstudiopaneldoor01.position.z = guicontrols.explode * -0.06;
          rrstudiopaneldoor01.visible = guicontrols.recordingroom;
        }
        if (typeof rrstudiopanelfloor01 !== "undefined") {
          rrstudiopanelfloor01.position.x = guicontrols.explode * -0.059;
          rrstudiopanelfloor01.position.y = guicontrols.explode * -0.115;
          rrstudiopanelfloor01.position.z = guicontrols.explode * 0.054;
          rrstudiopanelfloor01.visible = guicontrols.recordingroom;
        }
        if (typeof rrstudiopanelfloor02 !== "undefined") {
          rrstudiopanelfloor02.position.x = guicontrols.explode * -0.059;
          rrstudiopanelfloor02.position.y = guicontrols.explode * -0.115;
          rrstudiopanelfloor02.position.z = guicontrols.explode * -0.055;
          rrstudiopanelfloor02.visible = guicontrols.recordingroom;
        }
        if (typeof rrstudiopanelroof01 !== "undefined") {
          rrstudiopanelroof01.position.x = guicontrols.explode * -0.005;
          rrstudiopanelroof01.position.y = guicontrols.explode * 0.112;
          rrstudiopanelroof01.position.z = guicontrols.explode * -0.0;
          rrstudiopanelroof01.visible = guicontrols.roof;
        }
        if (typeof rrstudiopanelroof02 !== "undefined") {
          rrstudiopanelroof02.position.x = guicontrols.explode * -0.113;
          rrstudiopanelroof02.position.y = guicontrols.explode * 0.112;
          rrstudiopanelroof02.position.z = guicontrols.explode * -0.0;
          rrstudiopanelroof02.visible = guicontrols.roof;
        }
        if (typeof rrstudiopanelwall01 !== "undefined") {
          rrstudiopanelwall01.position.x = guicontrols.explode * -0.01;
          rrstudiopanelwall01.position.y = guicontrols.explode * -0.002;
          rrstudiopanelwall01.position.z = guicontrols.explode * -0.109;
          rrstudiopanelwall01.visible = guicontrols.recordingroom;
        }
        if (typeof rrstudiopanelwall02 !== "undefined") {
          rrstudiopanelwall02.position.x = guicontrols.explode * -0.119;
          rrstudiopanelwall02.position.y = guicontrols.explode * -0.002;
          rrstudiopanelwall02.position.z = guicontrols.explode * -0.109;
          rrstudiopanelwall02.visible = guicontrols.recordingroom;
        }
        if (typeof rrstudiopanelwall03 !== "undefined") {
          rrstudiopanelwall03.position.x = guicontrols.explode * -0.167;
          rrstudiopanelwall03.position.y = guicontrols.explode * -0.002;
          rrstudiopanelwall03.position.z = guicontrols.explode * -0.049;
          rrstudiopanelwall03.visible = guicontrols.recordingroom;
        }
        if (typeof rrstudiopanelwall04 !== "undefined") {
          rrstudiopanelwall04.position.x = guicontrols.explode * -0.167;
          rrstudiopanelwall04.position.y = guicontrols.explode * -0.002;
          rrstudiopanelwall04.position.z = guicontrols.explode * 0.059;
          rrstudiopanelwall04.visible = guicontrols.recordingroom;
        }
        if (typeof rrstudiopanelwall05 !== "undefined") {
          rrstudiopanelwall05.position.x = guicontrols.explode * -0.108;
          rrstudiopanelwall05.position.y = guicontrols.explode * -0.002;
          rrstudiopanelwall05.position.z = guicontrols.explode * 0.108;
          rrstudiopanelwall05.visible = guicontrols.recordingroom;
        }
        if (typeof rrstudiopanelwall06 !== "undefined") {
          rrstudiopanelwall06.position.x = guicontrols.explode * 0.001;
          rrstudiopanelwall06.position.y = guicontrols.explode * -0.002;
          rrstudiopanelwall06.position.z = guicontrols.explode * 0.108;
          rrstudiopanelwall06.visible = guicontrols.recordingroom;
        }
        if (typeof rrstudiopanelwindow01 !== "undefined") {
          rrstudiopanelwindow01.position.x = guicontrols.explode * 0.049;
          rrstudiopanelwindow01.position.y = guicontrols.explode * 0.002;
          rrstudiopanelwindow01.position.z = guicontrols.explode * 0.049;
          rrstudiopanelwindow01.visible = guicontrols.recordingroom;
        }

    }

    function animate() {
        requestAnimationFrame(animate);
        render();
        renderer.render(scene, camera);
    }

    init();
    animate();

    $(window).resize(function () {
        SCREEN_WIDTH = window.innerWidth;
        SCREEN_HEIGHT = window.innerHeight;
        camera.aspect = SCREEN_WIDTH / SCREEN_HEIGHT;
        camera.updateProjectionMatrix();
        renderer.setSize(SCREEN_WIDTH, SCREEN_HEIGHT);
    });

});
