$(function () {

    /*global variables*/
    var scene, camera, renderer;
    var controls, guicontrols, datGUI;
    var light1, light2, light3, light4, ambient;
    var geometry, material, sphere1, sphere2, sphere3, sphere4;
    var axisHelper;
    var loader;
    var SCREEN_WIDTH, SCREEN_HEIGHT;
    var mouse, raycaster, intersects;
    var objects = [];

    function init() {
        $(".popup").hide();
        $("#popup2").hide();

        /*creates empty scene object and renderer*/
        scene = new THREE.Scene();
        camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 0.1, 100);
        renderer = new THREE.WebGLRenderer({
            antialias: true,
            alpha: true
        });

        renderer.setSize(window.innerWidth, window.innerHeight);
        //renderer.shadowMapEnabled = false;
        //renderer.shadowMapSoft = false;

        /*add controls*/
        controls = new THREE.OrbitControls(camera, renderer.domElement);
        controls.addEventListener('change', render);

        camera.position.x = -15;
        camera.position.y = 15;
        camera.position.z = 2;
        camera.lookAt(scene.position);

        THREEx.WindowResize(renderer, camera);
        THREEx.FullScreen.bindKey({
            charCode: 'm'.charCodeAt(0)
        });

        /*datGUI controls object*/
        guicontrols = new function () {
            this.explodeModel = function () {
                console.log(scene);
            };
            this.explode = 0;
        }

        light1 = new THREE.PointLight(0x7d7d7d);
        light1.position.set(0, 100, -100);
        scene.add(light1);

        light2 = new THREE.PointLight(0x7d7d7d);
        light2.position.set(-100, 0, 0);
        scene.add(light2);

        light3 = new THREE.PointLight(0x7d7d7d);
        light3.position.set(0, -100, -100);
        scene.add(light3);

        light4 = new THREE.PointLight(0x7d7d7d);
        light4.position.set(0, -100, 100);
        scene.add(light4);

        ambient = new THREE.AmbientLight(0x7d7d7d);
        scene.add(ambient);

        
        //axisHelper to help with the positioning of the exploded parts
        /*
        axisHelper = new THREE.AxisHelper(10);
        scene.add(axisHelper);
        */
        
        loader = new THREE.ObjectLoader();
        loader.load("models/motor_v02_01.json", function (obj) {

            obj.scale.x = 5;
            obj.scale.y = 5;
            obj.scale.z = 5;
            obj.position.x = 0;
            obj.position.y = 0;
	        obj.position.z = -2;
            scene.add(obj);

            scene.traverse(function (children) {
                objects.push(children);
            });

	assemblybolts = obj.getObjectByName("assembly_bolts", true);
        axis12mm = obj.getObjectByName("axis_12mm", true);
        bearing12x28x8mm1 = obj.getObjectByName("bearing_12x28x8mm_1", true);
        bearing12x28x8mm2 = obj.getObjectByName("bearing_12x28x8mm_2", true);
        bearing12x28x8mm3 = obj.getObjectByName("bearing_12x28x8mm_3", true);
        bearingplate1 = obj.getObjectByName("bearing_plate_1", true);
        bearingplate2 = obj.getObjectByName("bearing_plate_2", true);
        bearingplatespacer1 = obj.getObjectByName("bearing_plate_spacer_1", true);
        bearingplatespacer2 = obj.getObjectByName("bearing_plate_spacer_2", true);
        impeller2 = obj.getObjectByName("impeller2", true);
        impeller1 = obj.getObjectByName("impeller_1", true);
        m4nutsthin1 = obj.getObjectByName("m4_nuts_thin_1", true);
        m4nutsthin2 = obj.getObjectByName("m4_nuts_thin_2", true);
        m4washers1 = obj.getObjectByName("m4_washers_1", true);
        m4washers2 = obj.getObjectByName("m4_washers_2", true);
        magnets1 = obj.getObjectByName("magnets_1", true);
        magnets2 = obj.getObjectByName("magnets_2", true);
        rotorplatemetal1 = obj.getObjectByName("rotor_plate_metal_1", true);
        rotorplatemetal2 = obj.getObjectByName("rotor_plate_metal_2", true);
        rotorplateplastic1 = obj.getObjectByName("rotor_plate_plastic_1", true);
        rotorplateplastic2 = obj.getObjectByName("rotor_plate_plastic_2", true);
        shaftcollar1 = obj.getObjectByName("shaft_collar_1", true);
        shaftcollar2 = obj.getObjectByName("shaft_collar_2", true);
        statorplateback = obj.getObjectByName("stator_plate_back", true);
        statorplatefront = obj.getObjectByName("stator_plate_front", true);


        });

        //add raycaster and mouse as 2D vector
        raycaster = new THREE.Raycaster();
        mouse = new THREE.Vector2();

        //add event listener for mouse and calls function when activated
        document.addEventListener('dblclick', onDoubleClick, false);
        document.addEventListener('mousemove', onDocumentMouseMove, false);
        document.addEventListener('touchstart', onDocumentTouchStart, false);

        /*adds controls to scene*/
        datGUI = new dat.GUI();
        datGUI.add(guicontrols, 'explodeModel');

        datGUI.add(guicontrols, 'explode', 0, 1);

        guicontrols.printedparts = true;
        datGUI.add(guicontrols, "printedparts").name("printed parts");

        guicontrols.assemblyboltsnutswashers = true;
        datGUI.add(guicontrols, "assemblyboltsnutswashers").name("assembly bolts nuts washers");

        guicontrols.shaftcollarsbearings = true;
        datGUI.add(guicontrols, "shaftcollarsbearings").name("shaft collars_bearings");

        guicontrols.metalrotorplates = true;
        datGUI.add(guicontrols, "metalrotorplates").name("metal rotor plates");

        guicontrols.magnets = true;
        datGUI.add(guicontrols, "magnets").name("magnets");

        guicontrols.motoraxis = true;
        datGUI.add(guicontrols, "motoraxis").name("motor axis");



        $("#webGL-container").append(renderer.domElement);

    }

    function onDocumentTouchStart(event) {

        event.preventDefault();

        event.clientX = event.touches[0].clientX;
        event.clientY = event.touches[0].clientY;
        onDoubleClick(event);
        onDocumentMouseMove(event);

    }

    function onDoubleClick(event) {

        event.preventDefault();

        mouse.x = (event.clientX / renderer.domElement.width) * 2 - 1;
        mouse.y = -(event.clientY / renderer.domElement.height) * 2 + 1;

        raycaster.setFromCamera(mouse, camera);

        intersects = raycaster.intersectObjects(objects);

        if (intersects.length > 0) {

            this.userData = intersects[0].object.userData;
            this.name = intersects[0].object.name;

            $(".text").empty();
            $(".popup").append("<div class='text'><p> <strong>" + this.name + "</strong>" + this.userData + "</div>");
            $(".popup").show();

        }

    }

    function onDocumentMouseMove(event) {

        event.preventDefault();

        mouse.x = (event.clientX / renderer.domElement.width) * 2 - 1;
        mouse.y = -(event.clientY / renderer.domElement.height) * 2 + 1;

        raycaster.setFromCamera(mouse, camera);

        var intersects = raycaster.intersectObjects(objects);

        if (intersects.length > 0) {

            this.name = intersects[0].object.name;

            $(".text2").empty();
            $("#popup2").append("<div class='text2'><p> <strong>" + this.name + "</strong></p></div>");
            $("#popup2").show();

        } else {
            $("#popup2").hide();

        }
    }

    function render() {
        if (typeof assemblybolts !== "undefined") {
          assemblybolts.position.x = guicontrols.explode * 0.0;
          assemblybolts.position.y = guicontrols.explode * 0.0;
          assemblybolts.position.z = guicontrols.explode * -0.064*2;
          assemblybolts.visible = guicontrols.assemblyboltsnutswashers;
        }
        if (typeof axis12mm !== "undefined") {
          axis12mm.position.x = guicontrols.explode * -0.0;
          axis12mm.position.y = guicontrols.explode * -0.0;
          axis12mm.position.z = guicontrols.explode * -0.024*2;
          axis12mm.visible = guicontrols.motoraxis;
        }
        if (typeof bearing12x28x8mm1 !== "undefined") {
          bearing12x28x8mm1.position.x = guicontrols.explode * 0.0;
          bearing12x28x8mm1.position.y = guicontrols.explode * -0.0;
          bearing12x28x8mm1.position.z = guicontrols.explode * 0.284*2;
          bearing12x28x8mm1.visible = guicontrols.shaftcollarsbearings;
        }
        if (typeof bearing12x28x8mm2 !== "undefined") {
          bearing12x28x8mm2.position.x = guicontrols.explode * -0.0;
          bearing12x28x8mm2.position.y = guicontrols.explode * -0.0;
          bearing12x28x8mm2.position.z = guicontrols.explode * 1.043*2;
          bearing12x28x8mm2.visible = guicontrols.shaftcollarsbearings;
        }
        if (typeof bearing12x28x8mm3 !== "undefined") {
          bearing12x28x8mm3.position.x = guicontrols.explode * 0.0;
          bearing12x28x8mm3.position.y = guicontrols.explode * -0.0;
          bearing12x28x8mm3.position.z = guicontrols.explode * -1.047*2;
          bearing12x28x8mm3.visible = guicontrols.shaftcollarsbearings;
        }
        if (typeof bearingplate1 !== "undefined") {
          bearingplate1.position.x = guicontrols.explode * 0.0;
          bearingplate1.position.y = guicontrols.explode * 0.001;
          bearingplate1.position.z = guicontrols.explode * 0.957*2;
          bearingplate1.visible = guicontrols.printedparts;
        }
        if (typeof bearingplate2 !== "undefined") {
          bearingplate2.position.x = guicontrols.explode * 0.0;
          bearingplate2.position.y = guicontrols.explode * 0.001;
          bearingplate2.position.z = guicontrols.explode * -0.949*2;
          bearingplate2.visible = guicontrols.printedparts;
        }
        if (typeof bearingplatespacer1 !== "undefined") {
          bearingplatespacer1.position.x = guicontrols.explode * 0.005;
          bearingplatespacer1.position.y = guicontrols.explode * 0.0;
          bearingplatespacer1.position.z = guicontrols.explode * 0.671*2;
          bearingplatespacer1.visible = guicontrols.printedparts;
        }
        if (typeof bearingplatespacer2 !== "undefined") {
          bearingplatespacer2.position.x = guicontrols.explode * 0.0;
          bearingplatespacer2.position.y = guicontrols.explode * 0.0;
          bearingplatespacer2.position.z = guicontrols.explode * -0.664*2;
          bearingplatespacer2.visible = guicontrols.printedparts;
        }
        if (typeof impeller2 !== "undefined") {
          impeller2.position.x = guicontrols.explode * 0.0;
          impeller2.position.y = guicontrols.explode * -0.0;
          impeller2.position.z = guicontrols.explode * 0.775*2;
          impeller2.visible = guicontrols.printedparts;
        }
        if (typeof impeller1 !== "undefined") {
          impeller1.position.x = guicontrols.explode * 0.0;
          impeller1.position.y = guicontrols.explode * -0.0;
          impeller1.position.z = guicontrols.explode * -0.863*2;
          impeller1.visible = guicontrols.printedparts;
        }
        if (typeof m4nutsthin1 !== "undefined") {
          m4nutsthin1.position.x = guicontrols.explode * 0.0;
          m4nutsthin1.position.y = guicontrols.explode * -0.0;
          m4nutsthin1.position.z = guicontrols.explode * -1.203*2;
          m4nutsthin1.visible = guicontrols.assemblyboltsnutswashers;
        }
        if (typeof m4nutsthin2 !== "undefined") {
          m4nutsthin2.position.x = guicontrols.explode * 0.0;
          m4nutsthin2.position.y = guicontrols.explode * -0.0;
          m4nutsthin2.position.z = guicontrols.explode * 1.18*2;
          m4nutsthin2.visible = guicontrols.assemblyboltsnutswashers;
        }
        if (typeof m4washers1 !== "undefined") {
          m4washers1.position.x = guicontrols.explode * 0.0;
          m4washers1.position.y = guicontrols.explode * -0.0;
          m4washers1.position.z = guicontrols.explode * -1.123*2;
          m4washers1.visible = guicontrols.assemblyboltsnutswashers;
        }
        if (typeof m4washers2 !== "undefined") {
          m4washers2.position.x = guicontrols.explode * 0.0;
          m4washers2.position.y = guicontrols.explode * -0.0;
          m4washers2.position.z = guicontrols.explode * 1.118*2;
          m4washers2.visible = guicontrols.assemblyboltsnutswashers;
        }
        if (typeof magnets1 !== "undefined") {
          magnets1.position.x = guicontrols.explode * 0.0;
          magnets1.position.y = guicontrols.explode * 0.0;
          magnets1.position.z = guicontrols.explode * -0.425*2;
          magnets1.visible = guicontrols.magnets;
        }
        if (typeof magnets2 !== "undefined") {
          magnets2.position.x = guicontrols.explode * 0.0;
          magnets2.position.y = guicontrols.explode * 0.0;
          magnets2.position.z = guicontrols.explode * 0.507*2;
          magnets2.visible = guicontrols.magnets;
        }
        if (typeof rotorplatemetal1 !== "undefined") {
          rotorplatemetal1.position.x = guicontrols.explode * -0.0;
          rotorplatemetal1.position.y = guicontrols.explode * 0.0;
          rotorplatemetal1.position.z = guicontrols.explode * -0.549*2;
          rotorplatemetal1.visible = guicontrols.metalrotorplates;
        }
        if (typeof rotorplatemetal2 !== "undefined") {
          rotorplatemetal2.position.x = guicontrols.explode * -0.0;
          rotorplatemetal2.position.y = guicontrols.explode * 0.0;
          rotorplatemetal2.position.z = guicontrols.explode * 0.553*2;
          rotorplatemetal2.visible = guicontrols.metalrotorplates;
        }
        if (typeof rotorplateplastic1 !== "undefined") {
          rotorplateplastic1.position.x = guicontrols.explode * -0.001;
          rotorplateplastic1.position.y = guicontrols.explode * -0.0;
          rotorplateplastic1.position.z = guicontrols.explode * -0.491*2;
          rotorplateplastic1.visible = guicontrols.printedparts;
        }
        if (typeof rotorplateplastic2 !== "undefined") {
          rotorplateplastic2.position.x = guicontrols.explode * -0.001;
          rotorplateplastic2.position.y = guicontrols.explode * -0.0;
          rotorplateplastic2.position.z = guicontrols.explode * 0.455*2;
          rotorplateplastic2.visible = guicontrols.printedparts;
        }
        if (typeof shaftcollar1 !== "undefined") {
          shaftcollar1.position.x = guicontrols.explode * -0.005;
          shaftcollar1.position.y = guicontrols.explode * -0.0;
          shaftcollar1.position.z = guicontrols.explode * -0.77*2;
          shaftcollar1.visible = guicontrols.shaftcollarsbearings;
        }
        if (typeof shaftcollar2 !== "undefined") {
          shaftcollar2.position.x = guicontrols.explode * -0.005;
          shaftcollar2.position.y = guicontrols.explode * -0.0;
          shaftcollar2.position.z = guicontrols.explode * 0.884*2;
          shaftcollar2.visible = guicontrols.shaftcollarsbearings;
        }
        if (typeof statorplateback !== "undefined") {
          statorplateback.position.x = guicontrols.explode * 0.007;
          statorplateback.position.y = guicontrols.explode * -0.005;
          statorplateback.position.z = guicontrols.explode * -0.357*2;
          statorplateback.visible = guicontrols.printedparts;
        }
        if (typeof statorplatefront !== "undefined") {
          statorplatefront.position.x = guicontrols.explode * 0.0;
          statorplatefront.position.y = guicontrols.explode * 0.0;
          statorplatefront.position.z = guicontrols.explode * 0.376*2;
          statorplatefront.visible = guicontrols.printedparts;
        }

    }

    function animate() {
        requestAnimationFrame(animate);
        render();
        renderer.render(scene, camera);
    }

    init();
    animate();

    $(window).resize(function () {
        SCREEN_WIDTH = window.innerWidth;
        SCREEN_HEIGHT = window.innerHeight;
        camera.aspect = SCREEN_WIDTH / SCREEN_HEIGHT;
        camera.updateProjectionMatrix();
        renderer.setSize(SCREEN_WIDTH, SCREEN_HEIGHT);
    });

});