$(function () {

    /*global variables*/
    var scene, camera, renderer;
    var controls, guicontrols, datGUI;
    var light1, light2, light3, light4, ambient;
    var geometry, material, sphere1, sphere2, sphere3, sphere4;
    var axisHelper;
    var loader;
    var SCREEN_WIDTH, SCREEN_HEIGHT;
    var mouse, raycaster, intersects;
    var objects = [];

    function init() {
        $(".popup").hide();
        $("#popup2").hide();

        /*creates empty scene object and renderer*/
        scene = new THREE.Scene();
        camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 0.1, 100);
        renderer = new THREE.WebGLRenderer({
            antialias: true,
            alpha: true
        });

        renderer.setSize(window.innerWidth, window.innerHeight);
        //renderer.shadowMapEnabled = false;
        //renderer.shadowMapSoft = false;

        /*add controls*/
        controls = new THREE.OrbitControls(camera, renderer.domElement);
        controls.addEventListener('change', render);

        camera.position.x = -10;
        camera.position.y = 10;
        camera.position.z = 10;
        camera.lookAt(scene.position);

        THREEx.WindowResize(renderer, camera);
        THREEx.FullScreen.bindKey({
            charCode: 'm'.charCodeAt(0)
        });

        /*datGUI controls object*/
        guicontrols = new function () {
            this.explodeModel = function () {
                console.log(scene);
            };
            this.explode = 0;
        }

        light1 = new THREE.PointLight(0x7d7d7d);
        light1.position.set(0, 10, -10);
        scene.add(light1);

        light2 = new THREE.PointLight(0x7d7d7d);
        light2.position.set(-10, 0, 0);
        scene.add(light2);

        light3 = new THREE.PointLight(0x7d7d7d);
        light3.position.set(0, -10, -10);
        scene.add(light3);

        light4 = new THREE.PointLight(0x7d7d7d);
        light4.position.set(0, -10, 10);
        scene.add(light4);

        ambient = new THREE.AmbientLight(0xffffff);
        scene.add(ambient);

        //spheres to help with the positioning of the lights
        /*
        geometry = new THREE.SphereGeometry(50, 32, 32);
        material = new THREE.MeshBasicMaterial({
            color: 0xffff00
        });
        sphere1 = new THREE.Mesh(geometry, material);
        sphere1.position.set(-10, 0, 0)
        scene.add(sphere1);

        sphere2 = new THREE.Mesh(geometry, material);
        sphere2.position.set(0, 10, 10)
        scene.add(sphere2);

        sphere3 = new THREE.Mesh(geometry, material);
        sphere3.position.set(0, -10, -10)
        scene.add(sphere3);

        sphere4 = new THREE.Mesh(geometry, material);
        sphere4.position.set(0, -10, 10)
        scene.add(sphere4);
        */

        //axisHelper to help with the positioning of the exploded parts
        /*
        axisHelper = new THREE.AxisHelper(10);
        scene.add(axisHelper);
        */
        
        loader = new THREE.ObjectLoader();
        loader.load("models/D3D_circuit_mill.json", function (obj) {

            obj.scale.x = 15;
            obj.scale.y = 15;
            obj.scale.z = 15;
            obj.position.x = 0;
            obj.position.y = 0;
	    obj.position.z = 0;
            scene.add(obj);

            scene.traverse(function (children) {
                objects.push(children);
            });

	framebackside = obj.getObjectByName("frame   back side", true);
        framebottom = obj.getObjectByName("frame   bottom", true);
        framefrontside = obj.getObjectByName("frame   front side", true);
        frameleftside = obj.getObjectByName("frame   left side", true);
        framerightside = obj.getObjectByName("frame   right side", true);
        frametop = obj.getObjectByName("frame   top", true);
        mountedmotormotormount1 = obj.getObjectByName("mounted motor   motor mount 1", true);
        mountedmotormotormount2 = obj.getObjectByName("mounted motor   motor mount 2", true);
        mountedmotorspindlemotor = obj.getObjectByName("mounted motor   Spindle Motor", true);
        pcbholderbed = obj.getObjectByName("PCB holder   Bed", true);
        pcbholderclamp11 = obj.getObjectByName("PCB holder   Clamp 1 1", true);
        pcbholderclamp12 = obj.getObjectByName("PCB holder   Clamp 1 2", true);
        pcbholderclamp21 = obj.getObjectByName("PCB holder   Clamp 2 1", true);
        pcbholderclamp22 = obj.getObjectByName("PCB holder   Clamp 2 2", true);
        xaxis1carriage = obj.getObjectByName("X axis 1   Carriage", true);
        xaxis1idler = obj.getObjectByName("X axis 1   Idler", true);
        xaxis1motor = obj.getObjectByName("X axis 1   Motor", true);
        xaxis1motorside = obj.getObjectByName("X axis 1   Motor side", true);
        xaxis1rod1 = obj.getObjectByName("X axis 1   Rod 1", true);
        xaxis1rod2 = obj.getObjectByName("X axis 1   Rod 2", true);
        xaxis2carriage = obj.getObjectByName("X axis 2   Carriage", true);
        xaxis2endstopinterface = obj.getObjectByName("X axis 2   End Stop Interface", true);
        xaxis2idler = obj.getObjectByName("X axis 2   Idler", true);
        xaxis2motor = obj.getObjectByName("X axis 2   Motor", true);
        xaxis2motorside = obj.getObjectByName("X axis 2   Motor side", true);
        xaxis2rod1 = obj.getObjectByName("X axis 2   Rod 1", true);
        xaxis2rod2 = obj.getObjectByName("X axis 2   Rod 2", true);
        yaxis1carriage1 = obj.getObjectByName("Y axis 1   Carriage 1", true);
        yaxis1carriage2 = obj.getObjectByName("Y axis 1   Carriage 2", true);
        yaxis1endstopinterface = obj.getObjectByName("Y axis 1   End Stop Interface", true);
        yaxis1idler = obj.getObjectByName("Y axis 1   Idler", true);
        yaxis1motor = obj.getObjectByName("Y axis 1   Motor", true);
        yaxis1motorside = obj.getObjectByName("Y axis 1   Motor side", true);
        yaxis1rod1 = obj.getObjectByName("Y axis 1   Rod 1", true);
        yaxis1rod2 = obj.getObjectByName("Y axis 1   Rod 2", true);
        yaxis2carriage1 = obj.getObjectByName("Y axis 2   Carriage 1", true);
        yaxis2carriage2 = obj.getObjectByName("Y axis 2   Carriage 2", true);
        yaxis2idler = obj.getObjectByName("Y axis 2   Idler", true);
        yaxis2motor = obj.getObjectByName("Y axis 2   Motor", true);
        yaxis2motorside = obj.getObjectByName("Y axis 2   Motor side", true);
        yaxis2rod1 = obj.getObjectByName("Y axis 2   Rod 1", true);
        yaxis2rod2 = obj.getObjectByName("Y axis 2   Rod 2", true);
        zaxis1carriage = obj.getObjectByName("Z axis 1   Carriage", true);
        zaxis1endstopinterface = obj.getObjectByName("Z axis 1   End Stop Interface", true);
        zaxis1idler = obj.getObjectByName("Z axis 1   Idler", true);
        zaxis1motor = obj.getObjectByName("Z axis 1   Motor", true);
        zaxis1motorside = obj.getObjectByName("Z axis 1   Motor side", true);
        zaxis1rod1 = obj.getObjectByName("Z axis 1   Rod 1", true);
        zaxis1rod2 = obj.getObjectByName("Z axis 1   Rod 2", true);
        zaxis2carriage = obj.getObjectByName("Z axis 2   Carriage", true);
        zaxis2idler = obj.getObjectByName("Z axis 2   Idler", true);
        zaxis2motor = obj.getObjectByName("Z axis 2   Motor", true);
        zaxis2motorside = obj.getObjectByName("Z axis 2   Motor side", true);
        zaxis2rod1 = obj.getObjectByName("Z axis 2   Rod 1", true);
        zaxis2rod2 = obj.getObjectByName("Z axis 2   Rod 2", true);


        });

        //add raycaster and mouse as 2D vector
        raycaster = new THREE.Raycaster();
        mouse = new THREE.Vector2();

        //add event listener for mouse and calls function when activated
        document.addEventListener('dblclick', onDoubleClick, false);
        document.addEventListener('mousemove', onDocumentMouseMove, false);
        document.addEventListener('touchstart', onDocumentTouchStart, false);

        /*adds controls to scene*/
        datGUI = new dat.GUI();
        datGUI.add(guicontrols, 'explodeModel');

        datGUI.add(guicontrols, 'explode', 0, 1);

        guicontrols.frame = true;
        datGUI.add(guicontrols, "frame").name("Frame");

        guicontrols.mountedmoter = true;
        datGUI.add(guicontrols, "mountedmoter").name("Mounted motor");

        guicontrols.xaxis1 = true;
        datGUI.add(guicontrols, "xaxis1").name("X axis 1");

        guicontrols.xaxis2 = true;
        datGUI.add(guicontrols, "xaxis2").name("X axis 2");

        guicontrols.yaxis1 = true;
        datGUI.add(guicontrols, "yaxis1").name("Y axis 1");

        guicontrols.yaxis2 = true;
        datGUI.add(guicontrols, "yaxis2").name("Y axis 2");

        guicontrols.zaxis1 = true;
        datGUI.add(guicontrols, "zaxis1").name("Z axis 1");

        guicontrols.zaxis2 = true;
        datGUI.add(guicontrols, "zaxis2").name("Z axis 2");

        guicontrols.pcbholder = true;
        datGUI.add(guicontrols, "pcbholder").name("PCB holder");



        $("#webGL-container").append(renderer.domElement);

    }

    function onDocumentTouchStart(event) {

        event.preventDefault();

        event.clientX = event.touches[0].clientX;
        event.clientY = event.touches[0].clientY;
        onDoubleClick(event);
        onDocumentMouseMove(event);

    }

    function onDoubleClick(event) {

        event.preventDefault();

        mouse.x = (event.clientX / renderer.domElement.width) * 2 - 1;
        mouse.y = -(event.clientY / renderer.domElement.height) * 2 + 1;

        raycaster.setFromCamera(mouse, camera);

        intersects = raycaster.intersectObjects(objects);

        if (intersects.length > 0) {

            this.userData = intersects[0].object.userData;
            this.name = intersects[0].object.name;

            $(".text").empty();
            $(".popup").append("<div class='text'><p> <strong>" + this.name + "</strong>" + this.userData + "</div>");
            $(".popup").show();

        }

    }

    function onDocumentMouseMove(event) {

        event.preventDefault();

        mouse.x = (event.clientX / renderer.domElement.width) * 2 - 1;
        mouse.y = -(event.clientY / renderer.domElement.height) * 2 + 1;

        raycaster.setFromCamera(mouse, camera);

        var intersects = raycaster.intersectObjects(objects);

        if (intersects.length > 0) {

            this.name = intersects[0].object.name;

            $(".text2").empty();
            $("#popup2").append("<div class='text2'><p> <strong>" + this.name + "</strong></p></div>");
            $("#popup2").show();

        } else {
            $("#popup2").hide();

        }
    }

    function render() {
        if (typeof framebackside !== "undefined") {
          framebackside.position.x = guicontrols.explode * -0.241;
          framebackside.position.y = guicontrols.explode * -0.003;
          framebackside.position.z = guicontrols.explode * 0.001;
          framebackside.visible = guicontrols.frame;
        }
        if (typeof framebottom !== "undefined") {
          framebottom.position.x = guicontrols.explode * -0.001;
          framebottom.position.y = guicontrols.explode * -0.216;
          framebottom.position.z = guicontrols.explode * -0.001;
          framebottom.visible = guicontrols.frame;
        }
        if (typeof framefrontside !== "undefined") {
          framefrontside.position.x = guicontrols.explode * 0.244;
          framefrontside.position.y = guicontrols.explode * -0.003;
          framefrontside.position.z = guicontrols.explode * 0.001;
          framefrontside.visible = guicontrols.frame;
        }
        if (typeof frameleftside !== "undefined") {
          frameleftside.position.x = guicontrols.explode * 0.001;
          frameleftside.position.y = guicontrols.explode * -0.003;
          frameleftside.position.z = guicontrols.explode * 0.243;
          frameleftside.visible = guicontrols.frame;
        }
        if (typeof framerightside !== "undefined") {
          framerightside.position.x = guicontrols.explode * -0.019;
          framerightside.position.y = guicontrols.explode * -0.023;
          framerightside.position.z = guicontrols.explode * -0.242;
          framerightside.visible = guicontrols.frame;
        }
        if (typeof frametop !== "undefined") {
          frametop.position.x = guicontrols.explode * 0.001;
          frametop.position.y = guicontrols.explode * 0.23;
          frametop.position.z = guicontrols.explode * 0.001;
          frametop.visible = guicontrols.frame;
        }
        if (typeof mountedmotormotormount1 !== "undefined") {
          mountedmotormotormount1.position.x = guicontrols.explode * 0.006;
          mountedmotormotormount1.position.y = guicontrols.explode * -0.076;
          mountedmotormotormount1.position.z = guicontrols.explode * 0.004;
          mountedmotormotormount1.visible = guicontrols.mountedmoter;
        }
        if (typeof mountedmotormotormount2 !== "undefined") {
          mountedmotormotormount2.position.x = guicontrols.explode * 0.074;
          mountedmotormotormount2.position.y = guicontrols.explode * -0.076;
          mountedmotormotormount2.position.z = guicontrols.explode * -0.002;
          mountedmotormotormount2.visible = guicontrols.mountedmoter;
        }
        if (typeof mountedmotorspindlemotor !== "undefined") {
          mountedmotorspindlemotor.position.x = guicontrols.explode * 0.04;
          mountedmotorspindlemotor.position.y = guicontrols.explode * -0.103;
          mountedmotorspindlemotor.position.z = guicontrols.explode * 0.001;
          mountedmotorspindlemotor.visible = guicontrols.mountedmoter;
        }
        if (typeof pcbholderbed !== "undefined") {
          pcbholderbed.position.x = guicontrols.explode * 0.001;
          pcbholderbed.position.y = guicontrols.explode * -0.199;
          pcbholderbed.position.z = guicontrols.explode * 0.001;
          pcbholderbed.visible = guicontrols.pcbholder;
        }
        if (typeof pcbholderclamp11 !== "undefined") {
          pcbholderclamp11.position.x = guicontrols.explode * 0.027;
          pcbholderclamp11.position.y = guicontrols.explode * -0.176;
          pcbholderclamp11.position.z = guicontrols.explode * 0.071;
          pcbholderclamp11.visible = guicontrols.pcbholder;
        }
        if (typeof pcbholderclamp12 !== "undefined") {
          pcbholderclamp12.position.x = guicontrols.explode * 0.018;
          pcbholderclamp12.position.y = guicontrols.explode * -0.161;
          pcbholderclamp12.position.z = guicontrols.explode * 0.067;
          pcbholderclamp12.visible = guicontrols.pcbholder;
        }
        if (typeof pcbholderclamp21 !== "undefined") {
          pcbholderclamp21.position.x = guicontrols.explode * -0.024;
          pcbholderclamp21.position.y = guicontrols.explode * -0.176;
          pcbholderclamp21.position.z = guicontrols.explode * -0.07;
          pcbholderclamp21.visible = guicontrols.pcbholder;
        }
        if (typeof pcbholderclamp22 !== "undefined") {
          pcbholderclamp22.position.x = guicontrols.explode * -0.019;
          pcbholderclamp22.position.y = guicontrols.explode * -0.161;
          pcbholderclamp22.position.z = guicontrols.explode * -0.066;
          pcbholderclamp22.visible = guicontrols.pcbholder;
        }
        if (typeof xaxis1carriage !== "undefined") {
          xaxis1carriage.position.x = guicontrols.explode * 0.126;
          xaxis1carriage.position.y = guicontrols.explode * 0.086;
          xaxis1carriage.position.z = guicontrols.explode * 0.002;
          xaxis1carriage.visible = guicontrols.xaxis1;
        }
        if (typeof xaxis1idler !== "undefined") {
          xaxis1idler.position.x = guicontrols.explode * 0.127;
          xaxis1idler.position.y = guicontrols.explode * 0.084;
          xaxis1idler.position.z = guicontrols.explode * -0.202;
          xaxis1idler.visible = guicontrols.xaxis1;
        }
        if (typeof xaxis1motor !== "undefined") {
          xaxis1motor.position.x = guicontrols.explode * 0.083;
          xaxis1motor.position.y = guicontrols.explode * 0.09;
          xaxis1motor.position.z = guicontrols.explode * 0.184;
          xaxis1motor.visible = guicontrols.xaxis1;
        }
        if (typeof xaxis1motorside !== "undefined") {
          xaxis1motorside.position.x = guicontrols.explode * 0.127;
          xaxis1motorside.position.y = guicontrols.explode * 0.084;
          xaxis1motorside.position.z = guicontrols.explode * 0.193;
          xaxis1motorside.visible = guicontrols.xaxis1;
        }
        if (typeof xaxis1rod1 !== "undefined") {
          xaxis1rod1.position.x = guicontrols.explode * 0.127;
          xaxis1rod1.position.y = guicontrols.explode * 0.117;
          xaxis1rod1.position.z = guicontrols.explode * 0.0;
          xaxis1rod1.visible = guicontrols.xaxis1;
        }
        if (typeof xaxis1rod2 !== "undefined") {
          xaxis1rod2.position.x = guicontrols.explode * 0.127;
          xaxis1rod2.position.y = guicontrols.explode * 0.052;
          xaxis1rod2.position.z = guicontrols.explode * 0.0;
          xaxis1rod2.visible = guicontrols.xaxis1;
        }
        if (typeof xaxis2carriage !== "undefined") {
          xaxis2carriage.position.x = guicontrols.explode * -0.046;
          xaxis2carriage.position.y = guicontrols.explode * 0.084;
          xaxis2carriage.position.z = guicontrols.explode * 0.001;
          xaxis2carriage.visible = guicontrols.xaxis2;
        }
        if (typeof xaxis2endstopinterface !== "undefined") {
          xaxis2endstopinterface.position.x = guicontrols.explode * -0.028;
          xaxis2endstopinterface.position.y = guicontrols.explode * 0.087;
          xaxis2endstopinterface.position.z = guicontrols.explode * 0.137;
          xaxis2endstopinterface.visible = guicontrols.xaxis2;
        }
        if (typeof xaxis2idler !== "undefined") {
          xaxis2idler.position.x = guicontrols.explode * -0.047;
          xaxis2idler.position.y = guicontrols.explode * 0.084;
          xaxis2idler.position.z = guicontrols.explode * -0.203;
          xaxis2idler.visible = guicontrols.xaxis2;
        }
        if (typeof xaxis2motor !== "undefined") {
          xaxis2motor.position.x = guicontrols.explode * -0.003;
          xaxis2motor.position.y = guicontrols.explode * 0.08;
          xaxis2motor.position.z = guicontrols.explode * 0.182;
          xaxis2motor.visible = guicontrols.xaxis2;
        }
        if (typeof xaxis2motorside !== "undefined") {
          xaxis2motorside.position.x = guicontrols.explode * -0.046;
          xaxis2motorside.position.y = guicontrols.explode * 0.085;
          xaxis2motorside.position.z = guicontrols.explode * 0.192;
          xaxis2motorside.visible = guicontrols.xaxis2;
        }
        if (typeof xaxis2rod1 !== "undefined") {
          xaxis2rod1.position.x = guicontrols.explode * -0.046;
          xaxis2rod1.position.y = guicontrols.explode * 0.053;
          xaxis2rod1.position.z = guicontrols.explode * -0.001;
          xaxis2rod1.visible = guicontrols.xaxis2;
        }
        if (typeof xaxis2rod2 !== "undefined") {
          xaxis2rod2.position.x = guicontrols.explode * -0.046;
          xaxis2rod2.position.y = guicontrols.explode * 0.118;
          xaxis2rod2.position.z = guicontrols.explode * -0.001;
          xaxis2rod2.visible = guicontrols.xaxis2;
        }
        if (typeof yaxis1carriage1 !== "undefined") {
          yaxis1carriage1.position.x = guicontrols.explode * -0.03;
          yaxis1carriage1.position.y = guicontrols.explode * 0.081;
          yaxis1carriage1.position.z = guicontrols.explode * 0.224;
          yaxis1carriage1.visible = guicontrols.yaxis1;
        }
        if (typeof yaxis1carriage2 !== "undefined") {
          yaxis1carriage2.position.x = guicontrols.explode * 0.109;
          yaxis1carriage2.position.y = guicontrols.explode * 0.081;
          yaxis1carriage2.position.z = guicontrols.explode * 0.225;
          yaxis1carriage2.visible = guicontrols.yaxis1;
        }
        if (typeof yaxis1endstopinterface !== "undefined") {
          yaxis1endstopinterface.position.x = guicontrols.explode * -0.167;
          yaxis1endstopinterface.position.y = guicontrols.explode * 0.079;
          yaxis1endstopinterface.position.z = guicontrols.explode * 0.206;
          yaxis1endstopinterface.visible = guicontrols.yaxis1;
        }
        if (typeof yaxis1idler !== "undefined") {
          yaxis1idler.position.x = guicontrols.explode * 0.234;
          yaxis1idler.position.y = guicontrols.explode * 0.081;
          yaxis1idler.position.z = guicontrols.explode * 0.225;
          yaxis1idler.visible = guicontrols.yaxis1;
        }
        if (typeof yaxis1motor !== "undefined") {
          yaxis1motor.position.x = guicontrols.explode * -0.21;
          yaxis1motor.position.y = guicontrols.explode * 0.084;
          yaxis1motor.position.z = guicontrols.explode * 0.177;
          yaxis1motor.visible = guicontrols.yaxis1;
        }
        if (typeof yaxis1motorside !== "undefined") {
          yaxis1motorside.position.x = guicontrols.explode * -0.22;
          yaxis1motorside.position.y = guicontrols.explode * 0.08;
          yaxis1motorside.position.z = guicontrols.explode * 0.225;
          yaxis1motorside.visible = guicontrols.yaxis1;
        }
        if (typeof yaxis1rod1 !== "undefined") {
          yaxis1rod1.position.x = guicontrols.explode * 0.001;
          yaxis1rod1.position.y = guicontrols.explode * 0.113;
          yaxis1rod1.position.z = guicontrols.explode * 0.225;
          yaxis1rod1.visible = guicontrols.yaxis1;
        }
        if (typeof yaxis1rod2 !== "undefined") {
          yaxis1rod2.position.x = guicontrols.explode * 0.001;
          yaxis1rod2.position.y = guicontrols.explode * 0.048;
          yaxis1rod2.position.z = guicontrols.explode * 0.225;
          yaxis1rod2.visible = guicontrols.yaxis1;
        }
        if (typeof yaxis2carriage1 !== "undefined") {
          yaxis2carriage1.position.x = guicontrols.explode * 0.107;
          yaxis2carriage1.position.y = guicontrols.explode * 0.079;
          yaxis2carriage1.position.z = guicontrols.explode * -0.222;
          yaxis2carriage1.visible = guicontrols.yaxis2;
        }
        if (typeof yaxis2carriage2 !== "undefined") {
          yaxis2carriage2.position.x = guicontrols.explode * -0.032;
          yaxis2carriage2.position.y = guicontrols.explode * 0.08;
          yaxis2carriage2.position.z = guicontrols.explode * -0.224;
          yaxis2carriage2.visible = guicontrols.yaxis2;
        }
        if (typeof yaxis2idler !== "undefined") {
          yaxis2idler.position.x = guicontrols.explode * 0.232;
          yaxis2idler.position.y = guicontrols.explode * 0.081;
          yaxis2idler.position.z = guicontrols.explode * -0.226;
          yaxis2idler.visible = guicontrols.yaxis2;
        }
        if (typeof yaxis2motor !== "undefined") {
          yaxis2motor.position.x = guicontrols.explode * -0.212;
          yaxis2motor.position.y = guicontrols.explode * 0.075;
          yaxis2motor.position.z = guicontrols.explode * -0.177;
          yaxis2motor.visible = guicontrols.yaxis2;
        }
        if (typeof yaxis2motorside !== "undefined") {
          yaxis2motorside.position.x = guicontrols.explode * -0.218;
          yaxis2motorside.position.y = guicontrols.explode * 0.081;
          yaxis2motorside.position.z = guicontrols.explode * -0.226;
          yaxis2motorside.visible = guicontrols.yaxis2;
        }
        if (typeof yaxis2rod1 !== "undefined") {
          yaxis2rod1.position.x = guicontrols.explode * 0.001;
          yaxis2rod1.position.y = guicontrols.explode * 0.048;
          yaxis2rod1.position.z = guicontrols.explode * -0.226;
          yaxis2rod1.visible = guicontrols.yaxis2;
        }
        if (typeof yaxis2rod2 !== "undefined") {
          yaxis2rod2.position.x = guicontrols.explode * 0.001;
          yaxis2rod2.position.y = guicontrols.explode * 0.113;
          yaxis2rod2.position.z = guicontrols.explode * -0.226;
          yaxis2rod2.visible = guicontrols.yaxis2;
        }
        if (typeof zaxis1carriage !== "undefined") {
          zaxis1carriage.position.x = guicontrols.explode * -0.018;
          zaxis1carriage.position.y = guicontrols.explode * 0.085;
          zaxis1carriage.position.z = guicontrols.explode * -0.0;
          zaxis1carriage.visible = guicontrols.zaxis1;
        }
        if (typeof zaxis1endstopinterface !== "undefined") {
          zaxis1endstopinterface.position.x = guicontrols.explode * -0.036;
          zaxis1endstopinterface.position.y = guicontrols.explode * 0.143;
          zaxis1endstopinterface.position.z = guicontrols.explode * 0.003;
          zaxis1endstopinterface.visible = guicontrols.zaxis1;
        }
        if (typeof zaxis1idler !== "undefined") {
          zaxis1idler.position.x = guicontrols.explode * -0.018;
          zaxis1idler.position.y = guicontrols.explode * -0.078;
          zaxis1idler.position.z = guicontrols.explode * 0.001;
          zaxis1idler.visible = guicontrols.zaxis1;
        }
        if (typeof zaxis1motor !== "undefined") {
          zaxis1motor.position.x = guicontrols.explode * -0.061;
          zaxis1motor.position.y = guicontrols.explode * 0.188;
          zaxis1motor.position.z = guicontrols.explode * -0.005;
          zaxis1motor.visible = guicontrols.zaxis1;
        }
        if (typeof zaxis1motorside !== "undefined") {
          zaxis1motorside.position.x = guicontrols.explode * -0.017;
          zaxis1motorside.position.y = guicontrols.explode * 0.196;
          zaxis1motorside.position.z = guicontrols.explode * 0.001;
          zaxis1motorside.visible = guicontrols.zaxis1;
        }
        if (typeof zaxis1rod1 !== "undefined") {
          zaxis1rod1.position.x = guicontrols.explode * -0.017;
          zaxis1rod1.position.y = guicontrols.explode * 0.06;
          zaxis1rod1.position.z = guicontrols.explode * -0.032;
          zaxis1rod1.visible = guicontrols.zaxis1;
        }
        if (typeof zaxis1rod2 !== "undefined") {
          zaxis1rod2.position.x = guicontrols.explode * -0.017;
          zaxis1rod2.position.y = guicontrols.explode * 0.06;
          zaxis1rod2.position.z = guicontrols.explode * 0.033;
          zaxis1rod2.visible = guicontrols.zaxis1;
        }
        if (typeof zaxis2carriage !== "undefined") {
          zaxis2carriage.position.x = guicontrols.explode * 0.098;
          zaxis2carriage.position.y = guicontrols.explode * 0.085;
          zaxis2carriage.position.z = guicontrols.explode * 0.002;
          zaxis2carriage.visible = guicontrols.zaxis2;
        }
        if (typeof zaxis2idler !== "undefined") {
          zaxis2idler.position.x = guicontrols.explode * 0.098;
          zaxis2idler.position.y = guicontrols.explode * -0.078;
          zaxis2idler.position.z = guicontrols.explode * 0.001;
          zaxis2idler.visible = guicontrols.zaxis2;
        }
        if (typeof zaxis2motor !== "undefined") {
          zaxis2motor.position.x = guicontrols.explode * 0.145;
          zaxis2motor.position.y = guicontrols.explode * 0.186;
          zaxis2motor.position.z = guicontrols.explode * 0.005;
          zaxis2motor.visible = guicontrols.zaxis2;
        }
        if (typeof zaxis2motorside !== "undefined") {
          zaxis2motorside.position.x = guicontrols.explode * 0.097;
          zaxis2motorside.position.y = guicontrols.explode * 0.196;
          zaxis2motorside.position.z = guicontrols.explode * 0.001;
          zaxis2motorside.visible = guicontrols.zaxis2;
        }
        if (typeof zaxis2rod1 !== "undefined") {
          zaxis2rod1.position.x = guicontrols.explode * 0.097;
          zaxis2rod1.position.y = guicontrols.explode * 0.06;
          zaxis2rod1.position.z = guicontrols.explode * 0.034;
          zaxis2rod1.visible = guicontrols.zaxis2;
        }
        if (typeof zaxis2rod2 !== "undefined") {
          zaxis2rod2.position.x = guicontrols.explode * 0.097;
          zaxis2rod2.position.y = guicontrols.explode * 0.06;
          zaxis2rod2.position.z = guicontrols.explode * -0.032;
          zaxis2rod2.visible = guicontrols.zaxis2;
        }

    }

    function animate() {
        requestAnimationFrame(animate);
        render();
        renderer.render(scene, camera);
    }

    init();
    animate();

    $(window).resize(function () {
        SCREEN_WIDTH = window.innerWidth;
        SCREEN_HEIGHT = window.innerHeight;
        camera.aspect = SCREEN_WIDTH / SCREEN_HEIGHT;
        camera.updateProjectionMatrix();
        renderer.setSize(SCREEN_WIDTH, SCREEN_HEIGHT);
    });

});
