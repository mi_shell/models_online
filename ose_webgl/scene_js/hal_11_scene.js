$(function () {

    /*global variables*/
    var scene, camera, renderer;
    var controls, guicontrols, datGUI;
    var light1, light2, light3, light4, ambient;
    var geometry, material, sphere1, sphere2, sphere3, sphere4;
    var axisHelper;
    var loader;
    var SCREEN_WIDTH, SCREEN_HEIGHT;
    var mouse, raycaster, intersects;
    var objects = [];

    function init() {
        $(".popup").hide();
        $("#popup2").hide();

        /*creates empty scene object and renderer*/
        scene = new THREE.Scene();
        camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 0.1, 100);
        renderer = new THREE.WebGLRenderer({
            antialias: true,
            alpha: true
        });

        renderer.setSize(window.innerWidth, window.innerHeight);
        //renderer.shadowMapEnabled = false;
        //renderer.shadowMapSoft = false;

        /*add controls*/
        controls = new THREE.OrbitControls(camera, renderer.domElement);
        controls.addEventListener('change', render);

        camera.position.x = 0;
        camera.position.y = 3;
        camera.position.z = -15;
        camera.lookAt(scene.position);

        THREEx.WindowResize(renderer, camera);
        THREEx.FullScreen.bindKey({
            charCode: 'm'.charCodeAt(0)
        });

        /*datGUI controls object*/
        guicontrols = new function () {
            this.explodeModel = function () {
                console.log(scene);
            };
            this.explode = 0;
        }

        light1 = new THREE.PointLight(0x7d7d7d);
        light1.position.set(0, 10, -10);
        scene.add(light1);

        light2 = new THREE.PointLight(0x7d7d7d);
        light2.position.set(-10, 0, 0);
        scene.add(light2);

        light3 = new THREE.PointLight(0x7d7d7d);
        light3.position.set(0, -10, -10);
        scene.add(light3);

        light4 = new THREE.PointLight(0x7d7d7d);
        light4.position.set(0, -10, 10);
        scene.add(light4);

        ambient = new THREE.AmbientLight(0xffffff);
        scene.add(ambient);

        //spheres to help with the positioning of the lights
        /*
        geometry = new THREE.SphereGeometry(50, 32, 32);
        material = new THREE.MeshBasicMaterial({
            color: 0xffff00
        });
        sphere1 = new THREE.Mesh(geometry, material);
        sphere1.position.set(-10, 0, 0)
        scene.add(sphere1);

        sphere2 = new THREE.Mesh(geometry, material);
        sphere2.position.set(0, 10, 10)
        scene.add(sphere2);

        sphere3 = new THREE.Mesh(geometry, material);
        sphere3.position.set(0, -10, -10)
        scene.add(sphere3);

        sphere4 = new THREE.Mesh(geometry, material);
        sphere4.position.set(0, -10, 10)
        scene.add(sphere4);
        */

        //axisHelper to help with the positioning of the exploded parts
        /*
        axisHelper = new THREE.AxisHelper(10);
        scene.add(axisHelper);
        */
        
        loader = new THREE.ObjectLoader();
        loader.load("models/hal_11_01.json", function (obj) {

            obj.scale.x = 0.5;
            obj.scale.y = 0.5;
            obj.scale.z = 0.5;
            obj.position.x = 0;
            obj.position.y = 0;
	    obj.position.z = 10;
            scene.add(obj);

            scene.traverse(function (children) {
                objects.push(children);
            });

	eersteverdieping = obj.getObjectByName("eerste_verdieping", true);
        gelijksvloers = obj.getObjectByName("gelijksvloers", true);


        });

        //add raycaster and mouse as 2D vector
        raycaster = new THREE.Raycaster();
        mouse = new THREE.Vector2();

        //add event listener for mouse and calls function when activated
        document.addEventListener('dblclick', onDoubleClick, false);
        document.addEventListener('mousemove', onDocumentMouseMove, false);
        document.addEventListener('touchstart', onDocumentTouchStart, false);

        /*adds controls to scene*/
        datGUI = new dat.GUI();
        datGUI.add(guicontrols, 'explodeModel');

        datGUI.add(guicontrols, 'explode', 0, 1);

        guicontrols.toon1steverdieping = true;
        datGUI.add(guicontrols, "toon1steverdieping").name("Toon 1ste verdieping");

        guicontrols.toongelijkvloers = true;
        datGUI.add(guicontrols, "toongelijkvloers").name("Toon gelijkvloers");


        $("#webGL-container").append(renderer.domElement);

    }

    function onDocumentTouchStart(event) {

        event.preventDefault();

        event.clientX = event.touches[0].clientX;
        event.clientY = event.touches[0].clientY;
        onDoubleClick(event);
        onDocumentMouseMove(event);

    }

    function onDoubleClick(event) {

        event.preventDefault();

        mouse.x = (event.clientX / renderer.domElement.width) * 2 - 1;
        mouse.y = -(event.clientY / renderer.domElement.height) * 2 + 1;

        raycaster.setFromCamera(mouse, camera);

        intersects = raycaster.intersectObjects(objects);

        if (intersects.length > 0) {

            this.userData = intersects[0].object.userData;
            this.name = intersects[0].object.name;

            $(".text").empty();
            $(".popup").append("<div class='text'><p> <strong>" + this.name + "</strong>" + this.userData + "</div>");
            $(".popup").show();

        }

    }

    function onDocumentMouseMove(event) {

        event.preventDefault();

        mouse.x = (event.clientX / renderer.domElement.width) * 2 - 1;
        mouse.y = -(event.clientY / renderer.domElement.height) * 2 + 1;

        raycaster.setFromCamera(mouse, camera);

        var intersects = raycaster.intersectObjects(objects);

        if (intersects.length > 0) {

            this.name = intersects[0].object.name;

            $(".text2").empty();
            $("#popup2").append("<div class='text2'><p> <strong>" + this.name + "</strong></p></div>");
            $("#popup2").show();

        } else {
            $("#popup2").hide();

        }
    }

    function render() {
        if (typeof eersteverdieping !== "undefined") {
          eersteverdieping.position.x = guicontrols.explode * -0.0;
          eersteverdieping.position.y = guicontrols.explode * 11.536;
          eersteverdieping.position.z = guicontrols.explode * -0.0;
          eersteverdieping.visible = guicontrols.toon1steverdieping;
        }
        if (typeof gelijksvloers !== "undefined") {
          gelijksvloers.position.x = guicontrols.explode * 0.0;
          gelijksvloers.position.y = guicontrols.explode * 0.0;
          gelijksvloers.position.z = guicontrols.explode * -0.0;
          gelijksvloers.visible = guicontrols.toongelijkvloers;
        }

    }

    function animate() {
        requestAnimationFrame(animate);
        render();
        renderer.render(scene, camera);
    }

    init();
    animate();

    $(window).resize(function () {
        SCREEN_WIDTH = window.innerWidth;
        SCREEN_HEIGHT = window.innerHeight;
        camera.aspect = SCREEN_WIDTH / SCREEN_HEIGHT;
        camera.updateProjectionMatrix();
        renderer.setSize(SCREEN_WIDTH, SCREEN_HEIGHT);
    });

});
