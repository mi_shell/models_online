$(function () {

    /*global variables*/
    var scene, camera, renderer;
    var controls, guicontrols, datGUI;
    var light1, light2, light3, light4, ambient;
    var geometry, material, sphere1, sphere2, sphere3, sphere4;
    var axisHelper;
    var loader;
    var SCREEN_WIDTH, SCREEN_HEIGHT;
    var mouse, raycaster, intersects;
    var objects = [];

    function init() {
        $(".popup").hide();
        $("#popup2").hide();

        /*creates empty scene object and renderer*/
        scene = new THREE.Scene();
        camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 0.1, 100);
        renderer = new THREE.WebGLRenderer({
            antialias: true,
            alpha: true
        });

        renderer.setSize(window.innerWidth, window.innerHeight);
        //renderer.shadowMapEnabled = false;
        //renderer.shadowMapSoft = false;

        /*add controls*/
        controls = new THREE.OrbitControls(camera, renderer.domElement);
        controls.addEventListener('change', render);

        camera.position.x = -10;
        camera.position.y = 10;
        camera.position.z = 10;
        camera.lookAt(scene.position);

        THREEx.WindowResize(renderer, camera);
        THREEx.FullScreen.bindKey({
            charCode: 'm'.charCodeAt(0)
        });

        /*datGUI controls object*/
        guicontrols = new function () {
            this.explodeModel = function () {
                console.log(scene);
            };
            this.explode = 0;
        }

        light1 = new THREE.PointLight(0x7d7d7d);
        light1.position.set(0, 10, -10);
        scene.add(light1);

        light2 = new THREE.PointLight(0x7d7d7d);
        light2.position.set(-10, 0, 0);
        scene.add(light2);

        light3 = new THREE.PointLight(0x7d7d7d);
        light3.position.set(0, -10, -10);
        scene.add(light3);

        light4 = new THREE.PointLight(0x7d7d7d);
        light4.position.set(0, -10, 10);
        scene.add(light4);

        ambient = new THREE.AmbientLight(0xffffff);
        scene.add(ambient);

        //spheres to help with the positioning of the lights
        /*
        geometry = new THREE.SphereGeometry(50, 32, 32);
        material = new THREE.MeshBasicMaterial({
            color: 0xffff00
        });
        sphere1 = new THREE.Mesh(geometry, material);
        sphere1.position.set(-10, 0, 0)
        scene.add(sphere1);

        sphere2 = new THREE.Mesh(geometry, material);
        sphere2.position.set(0, 10, 10)
        scene.add(sphere2);

        sphere3 = new THREE.Mesh(geometry, material);
        sphere3.position.set(0, -10, -10)
        scene.add(sphere3);

        sphere4 = new THREE.Mesh(geometry, material);
        sphere4.position.set(0, -10, 10)
        scene.add(sphere4);
        */

        //axisHelper to help with the positioning of the exploded parts
        /*
        axisHelper = new THREE.AxisHelper(10);
        scene.add(axisHelper);
        */
        
        loader = new THREE.ObjectLoader();
        loader.load("models/d3d_01.json", function (obj) {

            obj.scale.x = 15;
            obj.scale.y = 15;
            obj.scale.z = 15;
            obj.position.x = 0;
            obj.position.y = -1.2;
	    obj.position.z = 0;
            scene.add(obj);

            scene.traverse(function (children) {
                objects.push(children);
            });

	bedclamp1 = obj.getObjectByName("Bed clamp 1", true);
        bedclamp2 = obj.getObjectByName("Bed clamp 2", true);
        bedraiser1 = obj.getObjectByName("Bed Raiser 1", true);
        bedraiser2 = obj.getObjectByName("Bed Raiser 2", true);
        bedraiser3 = obj.getObjectByName("Bed Raiser 3", true);
        bedraiser4 = obj.getObjectByName("Bed Raiser 4", true);
        bedraiser5 = obj.getObjectByName("Bed Raiser 5", true);
        bedraiser6 = obj.getObjectByName("Bed Raiser 6", true);
        bedrod1 = obj.getObjectByName("Bed Rod 1", true);
        bedrod2 = obj.getObjectByName("Bed Rod 2", true);
        cablechain = obj.getObjectByName("Cable Chain", true);
        cablechainbracket = obj.getObjectByName("Cable chain  bracket", true);
        cablechainy = obj.getObjectByName("Cable Chain Y", true);
        cableholder1 = obj.getObjectByName("Cable holder 1", true);
        cableholder2 = obj.getObjectByName("Cable holder 2", true);
        extruderstripped = obj.getObjectByName("Extruder   Stripped", true);
        frame = obj.getObjectByName("Frame", true);
        heatedplate = obj.getObjectByName("Heated plate", true);
        xbracket1 = obj.getObjectByName("X Bracket 1", true);
        xbracket2 = obj.getObjectByName("X Bracket 2", true);
        xendstop = obj.getObjectByName("X Endstop", true);
        xfullcarriage = obj.getObjectByName("X Full Carriage", true);
        xhalfcarriage = obj.getObjectByName("X half carriage", true);
        xidler = obj.getObjectByName("X Idler", true);
        xmotor = obj.getObjectByName("X motor", true);
        xmotorside = obj.getObjectByName("X Motor Side", true);
        xrod1 = obj.getObjectByName("X Rod 1", true);
        xrod2 = obj.getObjectByName("X Rod 2", true);
        yendstop = obj.getObjectByName("Y Endstop", true);
        y1bracket = obj.getObjectByName("Y1 Bracket", true);
        y1fullcarriage = obj.getObjectByName("Y1 Full Carriage", true);
        y1idler = obj.getObjectByName("Y1 Idler", true);
        y1motor = obj.getObjectByName("Y1 motor", true);
        y1motorside = obj.getObjectByName("Y1 Motor Side", true);
        y1rod1 = obj.getObjectByName("Y1 Rod 1", true);
        y1rod2 = obj.getObjectByName("Y1 Rod 2", true);
        y2bracket = obj.getObjectByName("Y2 Bracket", true);
        y2fullcarriage = obj.getObjectByName("Y2 Full Carriage", true);
        y2idler = obj.getObjectByName("Y2 Idler", true);
        y2motor = obj.getObjectByName("Y2 motor", true);
        y2motorside = obj.getObjectByName("Y2 Motor Side", true);
        y2rod1 = obj.getObjectByName("Y2 Rod 1", true);
        y2rod2 = obj.getObjectByName("Y2 Rod 2", true);
        z1halfcarriage = obj.getObjectByName("Z1 Half Carriage", true);
        z1idler = obj.getObjectByName("Z1 Idler", true);
        z1motor = obj.getObjectByName("Z1 motor", true);
        z1motorpiece = obj.getObjectByName("Z1 Motor Piece", true);
        z1rod1 = obj.getObjectByName("Z1 Rod 1", true);
        z1rod2 = obj.getObjectByName("Z1 Rod 2", true);
        z1spacer1 = obj.getObjectByName("z1 spacer 1", true);
        z1spacer2 = obj.getObjectByName("z1 spacer 2", true);
        z2halfcarriage = obj.getObjectByName("Z2 Half Carriage", true);
        z2idler = obj.getObjectByName("Z2 Idler", true);
        z2motor = obj.getObjectByName("Z2 motor", true);
        z2motorpiece = obj.getObjectByName("Z2 Motor Piece", true);
        z2rod1 = obj.getObjectByName("Z2 Rod 1", true);
        z2rod2 = obj.getObjectByName("Z2 Rod 2", true);
        z2spacer1 = obj.getObjectByName("z2 spacer 1", true);
        z2spacer2 = obj.getObjectByName("z2 spacer 2", true);


        });

        //add raycaster and mouse as 2D vector
        raycaster = new THREE.Raycaster();
        mouse = new THREE.Vector2();

        //add event listener for mouse and calls function when activated
        document.addEventListener('dblclick', onDoubleClick, false);
        document.addEventListener('mousemove', onDocumentMouseMove, false);
        document.addEventListener('touchstart', onDocumentTouchStart, false);

        /*adds controls to scene*/
        datGUI = new dat.GUI();
        datGUI.add(guicontrols, 'explodeModel');

        datGUI.add(guicontrols, 'explode', 0, 1);

        guicontrols.frame = true;
        datGUI.add(guicontrols, "frame").name("Frame");

        guicontrols.bed = true;
        datGUI.add(guicontrols, "bed").name("Bed");

        guicontrols.cabelchains = true;
        datGUI.add(guicontrols, "cabelchains").name("Cabel chains");

        guicontrols.extruder = true;
        datGUI.add(guicontrols, "extruder").name("Extruder");

        guicontrols.axis = true;
        datGUI.add(guicontrols, "axis").name("Axis");



        $("#webGL-container").append(renderer.domElement);

    }

    function onDocumentTouchStart(event) {

        event.preventDefault();

        event.clientX = event.touches[0].clientX;
        event.clientY = event.touches[0].clientY;
        onDoubleClick(event);
        onDocumentMouseMove(event);

    }

    function onDoubleClick(event) {

        event.preventDefault();

        mouse.x = (event.clientX / renderer.domElement.width) * 2 - 1;
        mouse.y = -(event.clientY / renderer.domElement.height) * 2 + 1;

        raycaster.setFromCamera(mouse, camera);

        intersects = raycaster.intersectObjects(objects);

        if (intersects.length > 0) {

            this.userData = intersects[0].object.userData;
            this.name = intersects[0].object.name;

            $(".text").empty();
            $(".popup").append("<div class='text'><p> <strong>" + this.name + "</strong>" + this.userData + "</div>");
            $(".popup").show();

        }

    }

    function onDocumentMouseMove(event) {

        event.preventDefault();

        mouse.x = (event.clientX / renderer.domElement.width) * 2 - 1;
        mouse.y = -(event.clientY / renderer.domElement.height) * 2 + 1;

        raycaster.setFromCamera(mouse, camera);

        var intersects = raycaster.intersectObjects(objects);

        if (intersects.length > 0) {

            this.name = intersects[0].object.name;

            $(".text2").empty();
            $("#popup2").append("<div class='text2'><p> <strong>" + this.name + "</strong></p></div>");
            $("#popup2").show();

        } else {
            $("#popup2").hide();

        }
    }

    function render() {
        if (typeof bedclamp1 !== "undefined") {
          bedclamp1.position.x = guicontrols.explode * 0.13;
          bedclamp1.position.y = guicontrols.explode * -0.099;
          bedclamp1.position.z = guicontrols.explode * 0.001;
          bedclamp1.visible = guicontrols.bed;
        }
        if (typeof bedclamp2 !== "undefined") {
          bedclamp2.position.x = guicontrols.explode * -0.139;
          bedclamp2.position.y = guicontrols.explode * -0.098;
          bedclamp2.position.z = guicontrols.explode * 0.0;
          bedclamp2.visible = guicontrols.bed;
        }
        if (typeof bedraiser1 !== "undefined") {
          bedraiser1.position.x = guicontrols.explode * 0.045;
          bedraiser1.position.y = guicontrols.explode * -0.172;
          bedraiser1.position.z = guicontrols.explode * -0.001;
          bedraiser1.visible = guicontrols.bed;
        }
        if (typeof bedraiser2 !== "undefined") {
          bedraiser2.position.x = guicontrols.explode * -0.03;
          bedraiser2.position.y = guicontrols.explode * -0.172;
          bedraiser2.position.z = guicontrols.explode * 0.0;
          bedraiser2.visible = guicontrols.bed;
        }
        if (typeof bedraiser3 !== "undefined") {
          bedraiser3.position.x = guicontrols.explode * 0.044;
          bedraiser3.position.y = guicontrols.explode * -0.138;
          bedraiser3.position.z = guicontrols.explode * 0.0;
          bedraiser3.visible = guicontrols.bed;
        }
        if (typeof bedraiser4 !== "undefined") {
          bedraiser4.position.x = guicontrols.explode * -0.03;
          bedraiser4.position.y = guicontrols.explode * -0.139;
          bedraiser4.position.z = guicontrols.explode * 0.0;
          bedraiser4.visible = guicontrols.bed;
        }
        if (typeof bedraiser5 !== "undefined") {
          bedraiser5.position.x = guicontrols.explode * -0.03;
          bedraiser5.position.y = guicontrols.explode * -0.119;
          bedraiser5.position.z = guicontrols.explode * -0.0;
          bedraiser5.visible = guicontrols.bed;
        }
        if (typeof bedraiser6 !== "undefined") {
          bedraiser6.position.x = guicontrols.explode * 0.044;
          bedraiser6.position.y = guicontrols.explode * -0.119;
          bedraiser6.position.z = guicontrols.explode * 0.0;
          bedraiser6.visible = guicontrols.bed;
        }
        if (typeof bedrod1 !== "undefined") {
          bedrod1.position.x = guicontrols.explode * 0.004;
          bedrod1.position.y = guicontrols.explode * -0.156;
          bedrod1.position.z = guicontrols.explode * 0.022;
          bedrod1.visible = guicontrols.bed;
        }
        if (typeof bedrod2 !== "undefined") {
          bedrod2.position.x = guicontrols.explode * 0.004;
          bedrod2.position.y = guicontrols.explode * -0.157;
          bedrod2.position.z = guicontrols.explode * -0.025;
          bedrod2.visible = guicontrols.bed;
        }
        if (typeof cablechain !== "undefined") {
          cablechain.position.x = guicontrols.explode * -0.009;
          cablechain.position.y = guicontrols.explode * 0.243;
          cablechain.position.z = guicontrols.explode * -0.009;
          cablechain.visible = guicontrols.cabelchains;
        }
        if (typeof cablechainbracket !== "undefined") {
          cablechainbracket.position.x = guicontrols.explode * 0.01;
          cablechainbracket.position.y = guicontrols.explode * 0.087;
          cablechainbracket.position.z = guicontrols.explode * 0.106;
          cablechainbracket.visible = guicontrols.cabelchains;
        }
        if (typeof cablechainy !== "undefined") {
          cablechainy.position.x = guicontrols.explode * -0.056;
          cablechainy.position.y = guicontrols.explode * -0.025;
          cablechainy.position.z = guicontrols.explode * 0.116;
          cablechainy.visible = guicontrols.cabelchains;
        }
        if (typeof cableholder1 !== "undefined") {
          cableholder1.position.x = guicontrols.explode * 0.021;
          cableholder1.position.y = guicontrols.explode * 0.058;
          cableholder1.position.z = guicontrols.explode * 0.146;
          cableholder1.visible = guicontrols.cabelchains;
        }
        if (typeof cableholder2 !== "undefined") {
          cableholder2.position.x = guicontrols.explode * 0.016;
          cableholder2.position.y = guicontrols.explode * 0.085;
          cableholder2.position.z = guicontrols.explode * 0.122;
          cableholder2.visible = guicontrols.cabelchains;
        }
        if (typeof extruderstripped !== "undefined") {
          extruderstripped.position.x = guicontrols.explode * 0.047;
          extruderstripped.position.y = guicontrols.explode * 0.186;
          extruderstripped.position.z = guicontrols.explode * -0.128;
          extruderstripped.visible = guicontrols.extruder;
        }
        if (typeof frame !== "undefined") {
          frame.position.x = guicontrols.explode * -0.001;
          frame.position.y = guicontrols.explode * -0.004;
          frame.position.z = guicontrols.explode * -0.006;
          frame.visible = guicontrols.frame;
        }
        if (typeof heatedplate !== "undefined") {
          heatedplate.position.x = guicontrols.explode * 0.018;
          heatedplate.position.y = guicontrols.explode * -0.11;
          heatedplate.position.z = guicontrols.explode * 0.008;
          heatedplate.visible = guicontrols.bed;
        }
        if (typeof xbracket1 !== "undefined") {
          xbracket1.position.x = guicontrols.explode * 0.004;
          xbracket1.position.y = guicontrols.explode * 0.126;
          xbracket1.position.z = guicontrols.explode * -0.183;
          xbracket1.visible = guicontrols.axis;
        }
        if (typeof xbracket2 !== "undefined") {
          xbracket2.position.x = guicontrols.explode * 0.004;
          xbracket2.position.y = guicontrols.explode * 0.125;
          xbracket2.position.z = guicontrols.explode * 0.158;
          xbracket2.visible = guicontrols.axis;
        }
        if (typeof xendstop !== "undefined") {
          xendstop.position.x = guicontrols.explode * 0.023;
          xendstop.position.y = guicontrols.explode * 0.148;
          xendstop.position.z = guicontrols.explode * 0.147;
          xendstop.visible = guicontrols.axis;
        }
        if (typeof xfullcarriage !== "undefined") {
          xfullcarriage.position.x = guicontrols.explode * 0.005;
          xfullcarriage.position.y = guicontrols.explode * 0.177;
          xfullcarriage.position.z = guicontrols.explode * -0.086;
          xfullcarriage.visible = guicontrols.axis;
        }
        if (typeof xhalfcarriage !== "undefined") {
          xhalfcarriage.position.x = guicontrols.explode * 0.003;
          xhalfcarriage.position.y = guicontrols.explode * 0.15;
          xhalfcarriage.position.z = guicontrols.explode * -0.185;
          xhalfcarriage.visible = guicontrols.axis;
        }
        if (typeof xidler !== "undefined") {
          xidler.position.x = guicontrols.explode * 0.004;
          xidler.position.y = guicontrols.explode * 0.15;
          xidler.position.z = guicontrols.explode * -0.152;
          xidler.visible = guicontrols.axis;
        }
        if (typeof xmotor !== "undefined") {
          xmotor.position.x = guicontrols.explode * 0.009;
          xmotor.position.y = guicontrols.explode * 0.186;
          xmotor.position.z = guicontrols.explode * 0.181;
          xmotor.visible = guicontrols.axis;
        }
        if (typeof xmotorside !== "undefined") {
          xmotorside.position.x = guicontrols.explode * 0.004;
          xmotorside.position.y = guicontrols.explode * 0.15;
          xmotorside.position.z = guicontrols.explode * 0.181;
          xmotorside.visible = guicontrols.axis;
        }
        if (typeof xrod1 !== "undefined") {
          xrod1.position.x = guicontrols.explode * -0.02;
          xrod1.position.y = guicontrols.explode * 0.15;
          xrod1.position.z = guicontrols.explode * 0.0;
          xrod1.visible = guicontrols.axis;
        }
        if (typeof xrod2 !== "undefined") {
          xrod2.position.x = guicontrols.explode * 0.028;
          xrod2.position.y = guicontrols.explode * 0.15;
          xrod2.position.z = guicontrols.explode * 0.006;
          xrod2.visible = guicontrols.axis;
        }
        if (typeof yendstop !== "undefined") {
          yendstop.position.x = guicontrols.explode * -0.146;
          yendstop.position.y = guicontrols.explode * 0.082;
          yendstop.position.z = guicontrols.explode * 0.143;
          yendstop.visible = guicontrols.axis;
        }
        if (typeof y1bracket !== "undefined") {
          y1bracket.position.x = guicontrols.explode * -0.151;
          y1bracket.position.y = guicontrols.explode * 0.101;
          y1bracket.position.z = guicontrols.explode * 0.111;
          y1bracket.visible = guicontrols.axis;
        }
        if (typeof y1fullcarriage !== "undefined") {
          y1fullcarriage.position.x = guicontrols.explode * 0.005;
          y1fullcarriage.position.y = guicontrols.explode * 0.1;
          y1fullcarriage.position.z = guicontrols.explode * 0.141;
          y1fullcarriage.visible = guicontrols.axis;
        }
        if (typeof y1idler !== "undefined") {
          y1idler.position.x = guicontrols.explode * 0.127;
          y1idler.position.y = guicontrols.explode * 0.1;
          y1idler.position.z = guicontrols.explode * 0.14;
          y1idler.visible = guicontrols.axis;
        }
        if (typeof y1motor !== "undefined") {
          y1motor.position.x = guicontrols.explode * -0.179;
          y1motor.position.y = guicontrols.explode * 0.096;
          y1motor.position.z = guicontrols.explode * 0.175;
          y1motor.visible = guicontrols.axis;
        }
        if (typeof y1motorside !== "undefined") {
          y1motorside.position.x = guicontrols.explode * -0.181;
          y1motorside.position.y = guicontrols.explode * 0.101;
          y1motorside.position.z = guicontrols.explode * 0.14;
          y1motorside.visible = guicontrols.axis;
        }
        if (typeof y1rod1 !== "undefined") {
          y1rod1.position.x = guicontrols.explode * -0.028;
          y1rod1.position.y = guicontrols.explode * 0.076;
          y1rod1.position.z = guicontrols.explode * 0.141;
          y1rod1.visible = guicontrols.axis;
        }
        if (typeof y1rod2 !== "undefined") {
          y1rod2.position.x = guicontrols.explode * -0.032;
          y1rod2.position.y = guicontrols.explode * 0.125;
          y1rod2.position.z = guicontrols.explode * 0.141;
          y1rod2.visible = guicontrols.axis;
        }
        if (typeof y2bracket !== "undefined") {
          y2bracket.position.x = guicontrols.explode * -0.15;
          y2bracket.position.y = guicontrols.explode * 0.099;
          y2bracket.position.z = guicontrols.explode * -0.124;
          y2bracket.visible = guicontrols.axis;
        }
        if (typeof y2fullcarriage !== "undefined") {
          y2fullcarriage.position.x = guicontrols.explode * 0.003;
          y2fullcarriage.position.y = guicontrols.explode * 0.101;
          y2fullcarriage.position.z = guicontrols.explode * -0.153;
          y2fullcarriage.visible = guicontrols.axis;
        }
        if (typeof y2idler !== "undefined") {
          y2idler.position.x = guicontrols.explode * 0.125;
          y2idler.position.y = guicontrols.explode * 0.1;
          y2idler.position.z = guicontrols.explode * -0.153;
          y2idler.visible = guicontrols.axis;
        }
        if (typeof y2motor !== "undefined") {
          y2motor.position.x = guicontrols.explode * -0.18;
          y2motor.position.y = guicontrols.explode * 0.104;
          y2motor.position.z = guicontrols.explode * -0.187;
          y2motor.visible = guicontrols.axis;
        }
        if (typeof y2motorside !== "undefined") {
          y2motorside.position.x = guicontrols.explode * -0.181;
          y2motorside.position.y = guicontrols.explode * 0.098;
          y2motorside.position.z = guicontrols.explode * -0.153;
          y2motorside.visible = guicontrols.axis;
        }
        if (typeof y2rod1 !== "undefined") {
          y2rod1.position.x = guicontrols.explode * -0.031;
          y2rod1.position.y = guicontrols.explode * 0.125;
          y2rod1.position.z = guicontrols.explode * -0.153;
          y2rod1.visible = guicontrols.axis;
        }
        if (typeof y2rod2 !== "undefined") {
          y2rod2.position.x = guicontrols.explode * -0.027;
          y2rod2.position.y = guicontrols.explode * 0.076;
          y2rod2.position.z = guicontrols.explode * -0.153;
          y2rod2.visible = guicontrols.axis;
        }
        if (typeof z1halfcarriage !== "undefined") {
          z1halfcarriage.position.x = guicontrols.explode * 0.166;
          z1halfcarriage.position.y = guicontrols.explode * -0.099;
          z1halfcarriage.position.z = guicontrols.explode * -0.001;
          z1halfcarriage.visible = guicontrols.axis;
        }
        if (typeof z1idler !== "undefined") {
          z1idler.position.x = guicontrols.explode * 0.166;
          z1idler.position.y = guicontrols.explode * -0.133;
          z1idler.position.z = guicontrols.explode * -0.0;
          z1idler.visible = guicontrols.axis;
        }
        if (typeof z1motor !== "undefined") {
          z1motor.position.x = guicontrols.explode * 0.2;
          z1motor.position.y = guicontrols.explode * 0.143;
          z1motor.position.z = guicontrols.explode * 0.003;
          z1motor.visible = guicontrols.axis;
        }
        if (typeof z1motorpiece !== "undefined") {
          z1motorpiece.position.x = guicontrols.explode * 0.166;
          z1motorpiece.position.y = guicontrols.explode * 0.157;
          z1motorpiece.position.z = guicontrols.explode * -0.001;
          z1motorpiece.visible = guicontrols.axis;
        }
        if (typeof z1rod1 !== "undefined") {
          z1rod1.position.x = guicontrols.explode * 0.166;
          z1rod1.position.y = guicontrols.explode * 0.009;
          z1rod1.position.z = guicontrols.explode * 0.024;
          z1rod1.visible = guicontrols.axis;
        }
        if (typeof z1rod2 !== "undefined") {
          z1rod2.position.x = guicontrols.explode * 0.166;
          z1rod2.position.y = guicontrols.explode * 0.009;
          z1rod2.position.z = guicontrols.explode * -0.025;
          z1rod2.visible = guicontrols.axis;
        }
        if (typeof z1spacer1 !== "undefined") {
          z1spacer1.position.x = guicontrols.explode * 0.145;
          z1spacer1.position.y = guicontrols.explode * 0.127;
          z1spacer1.position.z = guicontrols.explode * -0.001;
          z1spacer1.visible = guicontrols.axis;
        }
        if (typeof z1spacer2 !== "undefined") {
          z1spacer2.position.x = guicontrols.explode * 0.125;
          z1spacer2.position.y = guicontrols.explode * -0.122;
          z1spacer2.position.z = guicontrols.explode * -0.001;
          z1spacer2.visible = guicontrols.axis;
        }
        if (typeof z2halfcarriage !== "undefined") {
          z2halfcarriage.position.x = guicontrols.explode * -0.169;
          z2halfcarriage.position.y = guicontrols.explode * -0.099;
          z2halfcarriage.position.z = guicontrols.explode * -0.001;
          z2halfcarriage.visible = guicontrols.axis;
        }
        if (typeof z2idler !== "undefined") {
          z2idler.position.x = guicontrols.explode * -0.168;
          z2idler.position.y = guicontrols.explode * -0.133;
          z2idler.position.z = guicontrols.explode * -0.003;
          z2idler.visible = guicontrols.axis;
        }
        if (typeof z2motor !== "undefined") {
          z2motor.position.x = guicontrols.explode * -0.205;
          z2motor.position.y = guicontrols.explode * 0.14;
          z2motor.position.z = guicontrols.explode * -0.005;
          z2motor.visible = guicontrols.axis;
        }
        if (typeof z2motorpiece !== "undefined") {
          z2motorpiece.position.x = guicontrols.explode * -0.169;
          z2motorpiece.position.y = guicontrols.explode * 0.156;
          z2motorpiece.position.z = guicontrols.explode * -0.002;
          z2motorpiece.visible = guicontrols.axis;
        }
        if (typeof z2rod1 !== "undefined") {
          z2rod1.position.x = guicontrols.explode * -0.169;
          z2rod1.position.y = guicontrols.explode * 0.008;
          z2rod1.position.z = guicontrols.explode * -0.027;
          z2rod1.visible = guicontrols.axis;
        }
        if (typeof z2rod2 !== "undefined") {
          z2rod2.position.x = guicontrols.explode * -0.169;
          z2rod2.position.y = guicontrols.explode * 0.008;
          z2rod2.position.z = guicontrols.explode * 0.022;
          z2rod2.visible = guicontrols.axis;
        }
        if (typeof z2spacer1 !== "undefined") {
          z2spacer1.position.x = guicontrols.explode * -0.148;
          z2spacer1.position.y = guicontrols.explode * 0.127;
          z2spacer1.position.z = guicontrols.explode * -0.001;
          z2spacer1.visible = guicontrols.axis;
        }
        if (typeof z2spacer2 !== "undefined") {
          z2spacer2.position.x = guicontrols.explode * -0.134;
          z2spacer2.position.y = guicontrols.explode * -0.121;
          z2spacer2.position.z = guicontrols.explode * -0.001;
          z2spacer2.visible = guicontrols.axis;
        }

    }

    function animate() {
        requestAnimationFrame(animate);
        render();
        renderer.render(scene, camera);
    }

    init();
    animate();

    $(window).resize(function () {
        SCREEN_WIDTH = window.innerWidth;
        SCREEN_HEIGHT = window.innerHeight;
        camera.aspect = SCREEN_WIDTH / SCREEN_HEIGHT;
        camera.updateProjectionMatrix();
        renderer.setSize(SCREEN_WIDTH, SCREEN_HEIGHT);
    });

});
